#ifndef PERMUTATION_H_
#define PERMUTATION_H_

#include <stddef.h>

void print_permu(long unsigned *perm, const long unsigned n);

unsigned long factorial(const long unsigned n);

void lexicographical_permutation(long *dest,
                                 long unsigned perm_num,
                                 const long *part,
                                 size_t n);

#endif /* PERMUTATION_H_ */
