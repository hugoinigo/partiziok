#ifndef ARGS_H_
#define ARGS_H_
#include <getopt.h>
#include "util.h"

#define SIZE_ALPHA_IND 2
#define SIZE_BETA_IND 3
#define TIME_ALPHA_IND 5
#define TIME_BETA_IND 6
#define SPEED_FACTOR_IND 10
#define ROTATE_IND 11
#define MODE_IND 12

#define MANDATORY_ARGS 13
#define COLUMN_FILL 16

#define OPTIONS_SIZE (sizeof(options) / sizeof(struct option) - 1)

int parse_args(struct config *cfg, int argc, char **argv);

#endif /* ARGS_H_ */
