#include "mc.h"
#include "part.h"
#include "graph.h"
#include "gen.h"
#include "metric.h"
#include "array.h"

#include <stdio.h>
#include <limits.h>
#include <sys/time.h>

static long unsigned total_alloc = 0;
static long unsigned total_dealloc = 0;

static bool part_fits(const struct graph *g,
                      const long *coords,
                      const long *part)
{
        unsigned long i, res;
        long *iter = calloc(sizeof(long), g->n);

        res = 1;
        for (i = 0; i < g->n; ++i) {
                res *= part[i];
        }

        for (i = 0; i < res; ++i) {
                if (*graph_elem(g, coords, iter) != FREE) {
                        free(iter);

                        return false;
                }

                iter_coords(iter, part, g->n);
        }

        free(iter);
        return true;
}

static struct part *try_fit(struct graph *g,
                            const long *coords,
                            const long *part,
                            unsigned long id)
{
        size_t i;
        struct part *result;

        for (i = 0; i < g->n; ++i) {
                if (coords[i] + part[i] > g->k[i]) {
                        return NULL;
                }
        }

        if (!part_fits(g, coords, part)) {
                return NULL;
        }

        /* set_graph(g, coords, part, id); */

        result = malloc(sizeof(struct part));

        result->coords = malloc(sizeof(unsigned long) * g->n);
        result->dimensions = malloc(sizeof(unsigned long) * g->n);

        result->time = part[g->n];
        result->next = NULL;
        result->links = part_count_edges(part, g->n);

        for (i = 0; i < g->n; ++i) {
                result->coords[i] = coords[i];
                result->dimensions[i] = part[i];
        }

        return result;
}

static void update_utilization(struct graph *g)
{
        double cur_node_utilization, cur_edge_utilization;

        /* nodoen erabilpena */
        cur_node_utilization = (g->time - g->prev_time)
                * g->alloc_nodes / (double) g->len;
        g->node_utilization += cur_node_utilization;

        /* ertzen erabilpena */
        cur_edge_utilization = (g->time - g->prev_time)
                * g->alloc_edges / (double) g->e;
        g->edge_utilization += cur_edge_utilization;
}

static unsigned long count_part_nodes(const struct part *p, const unsigned long n)
{
        unsigned long i, acc = 1;
        for (i = 0; i < n; ++i) {
                acc *= p->dimensions[i];
        }

        return acc;
}

static void part_dec_time_by(struct part * const p, unsigned long num)
{
        struct part *i = p;
        while (i != NULL) {
                i->time -= num;
                i = i->next;
        }
}

static unsigned long part_min_time(const struct part *const p)
{
        unsigned long min = BIG_NUM;
        const struct part *i = p;

        while (i != NULL) {
                if (i->time < min) {
                        min = i->time;
                }
                i = i->next;
        }

        return min;
}

static unsigned long time_delta(struct timeval prev_tv, struct timeval post_tv)
{
	time_t delta_sec = post_tv.tv_sec - prev_tv.tv_sec;
	suseconds_t delta_usec = post_tv.tv_usec - prev_tv.tv_usec;
	return 1000000 * delta_sec + delta_usec;
}

static void part_remove_timeouts(FILE *s,
                                 const struct config *c,
                                 struct part **head,
                                 struct graph *g)
{
        struct part **i = head;
        struct part *next;
        long unsigned dealloc_delta;
        struct timeval prev_tv, post_tv;

        while (*i != NULL) {
                if ((*i)->time <= 0) {
                        g->alloc_nodes -= count_part_nodes(*i, g->n);
                        g->alloc_edges -= (*i)->links;

                        gettimeofday(&prev_tv, NULL);
                        set_graph(g, (*i)->coords, (*i)->dimensions, FREE);
                        gettimeofday(&post_tv, NULL);
                        dealloc_delta = time_delta(prev_tv, post_tv);

                        update_metric(&dealloc_time, dealloc_delta);
                        total_dealloc += dealloc_delta;

                        ++g->event;
                        if (c->verbose) {
                                fprintf(s, "D ");
                                fprintg(s, g);
                                fprintp(s, *i, g->n);
                        }

                        next = (*i)->next;
                        part_free(*i);
                        *i = next;
                } else {
                        i = &(*i)->next;
                }
        }
}

static void fprintm(FILE *restrict s,
                    const char *msg,
                    const struct metric *m)
{
        /* fprintf(s, "%s metric:\n", msg); */
        fprintf(s, "%s min: %lu\n", msg,
                m->min == ULONG_MAX ? 0 : m->min);
        fprintf(s, "%s max: %lu\n", msg, m->max);
        fprintf(s, "%s avg: %lf\n", msg, m->avg);
}

int monte_carlo(const struct config *cfg)
{
        /* printf("[Monte Carlo] kaixo egunon!!\n"); */
        unsigned stop_trials = 0;
        unsigned max_part_trials = 50;
        unsigned max_stop_trials = 1000;
        unsigned id = 1;
        char *fname;
        FILE *fd;

        gsl_rng *r1, *r2;

        long *coords;
        unsigned long i, j;
        long *p;

        unsigned long delta_time, part_nodes;

        bool part_allocated;
        bool all_trials;

        struct part *part;
        struct part *head = NULL;
        struct graph g;

        long unsigned alloc_delta = 0;
        struct timeval prev_tv, post_tv;
        
        fname = gen_filename(cfg);
        fd = fopen(fname, "w+");
        fprintf(fd, "%s\n", fname);

        r1 = gsl_rng_alloc(gsl_rng_taus);
        r2 = gsl_rng_alloc(gsl_rng_taus);
        
        gsl_rng_set(r1, cfg->seed + 1);
        gsl_rng_set(r2, cfg->seed + 2);
        srand(cfg->seed);

        graph_alloc(&g, cfg->k, cfg->n);

        p = malloc(sizeof(*p) * (cfg->n+1));
        coords = malloc(sizeof(*coords) * cfg->n);

        while (stop_trials < max_stop_trials) {
                g.prev_time = g.time;
                all_trials = true;
                for (i = 0; i < max_part_trials; ++i) {
                        /* partizio berri bat kalkulatu */
                        gen_rnd_partition(p, r1, r2, cfg);
                        part_allocated = false;
                        for (j = 0; j < cfg->n; ++j) {
                                coords[j] = 0;
                        }
                        do {
                                gettimeofday(&prev_tv, NULL);
                                part = try_fit(&g, coords, p, id);
                                gettimeofday(&post_tv, NULL);
                                alloc_delta += time_delta(prev_tv, post_tv);
                                if (part == NULL) {
                                        iter_coords(coords, cfg->k, cfg->n);
                                } else {
                                        g.time += 1;

                                        update_utilization(&g);
                                        part_nodes = count_part_nodes(part, g.n);

                                        gettimeofday(&prev_tv, NULL);
                                        set_graph(&g, part->coords, part->dimensions, id);
                                        gettimeofday(&post_tv, NULL);
                                        alloc_delta += time_delta(prev_tv, post_tv);

                                        update_metric(&psize, part_nodes);
                                        update_metric(&ptime, part->time);
                                        update_metric(&alloc_time, alloc_delta);

                                        total_alloc += alloc_delta;
                                        alloc_delta = 0;
                                        
                                        g.alloc_nodes += part_nodes;
                                        g.alloc_edges += part->links;
                                        
                                        part_append(&head, part);
                                        part_dec_time_by(head, 1);

                                        id++;
                                        ++g.event;
                                        if (cfg->verbose) {
                                                fprintf(fd, "A ");
                                                fprintg(fd, &g);
                                                fprintp(fd, part, g.n);
                                        }
                                        
                                        part_allocated = true;
                                        break;
                                }
                        } while (!array_all(coords, g.n, 0));

                        if (part_allocated) {
                                all_trials = false;
                                break;
                        }
                }
                if (all_trials) {
                        stop_trials++;
                        /* calculate next time */
                        delta_time = part_min_time(head);

                        /* update job wait metric */
                        update_metric(&pwait, delta_time);

                        /* decrease time */
                        part_dec_time_by(head, delta_time);

                        g.time += delta_time;
                        update_utilization(&g);
                }
                part_remove_timeouts(fd, cfg, &head, &g);
        }

        while (head != NULL) {
                delta_time = part_min_time(head);
                part_dec_time_by(head, delta_time);

                g.prev_time = g.time;
                g.time += delta_time;

                update_utilization(&g);
                part_remove_timeouts(fd, cfg, &head, &g);
        }
        printf("%s\n", fname);

        fprintf(fd, "node utilization: %lf\n", g.node_utilization / g.time);
        fprintf(fd, "edge utilization: %lf\n", g.edge_utilization / g.time);
        
        psize.avg /= g.event;
        fprintm(fd, "part size", &psize);
        
        ifrag.avg /= g.event;
        fprintm(fd, "real part size", &ifrag);

        ptime.avg /= g.event;
        fprintm(fd, "part time", &ptime);
        
        pwait.avg /= g.event;
        fprintm(fd, "part wait", &pwait);

        afails.avg /= g.event;
        fprintm(fd, "alloc fails", &afails);

        afails_rot.avg /= g.event;
        fprintm(fd, "alloc fails rot", &afails_rot);

        fprintf(fd, "simulation time: %lu\n", g.time);

        fprintf(fd, "simulation time: %lu\n", g.time);

        long unsigned total_time = total_alloc + total_dealloc;

	fprintf(fd, "alloc time: %lu\n", total_alloc);
        alloc_time.avg = (float) total_alloc / cfg->part_list_size;
	fprintm(fd, "alloc time", &alloc_time);

	fprintf(fd, "dealloc time: %lu\n", total_dealloc);
	dealloc_time.avg = (float) total_dealloc / cfg->part_list_size;
	fprintm(fd, "dealloc time", &dealloc_time);

        fprintf(fd, "total time: %lu\n", total_time);

	printf("alloc time avg: %.0lfus\n", alloc_time.avg);
	printf("dealloc time avg: %.0lfus\n", dealloc_time.avg);

        free(fname);
        part_free_all(&head);
        graph_free(&g);

        free(coords);
        free(p);
        return 0;
}
