import os
from enum import Enum
from subprocess import Popen

class Log(Enum):
    NONE = 0
    FULL = 4

class Target:
    def __init__(self, str, deps=[]):
        self.target = str
        self.deps = deps
        self.cmd_reset()

    def deps_set(self, deps):
        self.deps = deps

    def deps_append(self, dep):
        self.deps.append(dep)

    def cmd_append(self, cmd):
        self.cmd_list.append(cmd)
                             # .format(target=self.target,
                             #         deps=strcat(self.deps, sep=' ')))

    def cmd_reset(self):
        self.cmd_list = []

    def cmd_list_exec(self):
        for cmd in self.cmd_list:
            code = cmd.exec_sync()
            if code != 0:
                return code

    def debug(self):
        print(str(self))

    def __str__(self):
        res = ''
        res += 'target: ' + str(self.target) + '\n'
        res += 'deps: ' + str(self.deps) + '\n'
        res += 'cmd_list: ' + str(self.cmd_list)
        return res

    def exec(self, log=Log.NONE):
        if os.path.isdir(self.target):
            # print('nothing to do')
            return
            
        target_modif = 0.0
        if os.path.exists(self.target):
            target_modif += os.path.getmtime(self.target)
        # except OSError:
        else:
            self.cmd_list_exec()
            return

        if len(self.deps) == 0:
            self.cmd_list_exec()
            return

        for dep in self.deps:
            dep_modif = os.path.getmtime(dep)
            if target_modif < dep_modif:
                self.cmd_list_exec()
                return
        # print('nothing to do')

def str_split(string, sep=' '):
    return list(filter(lambda s: s != '', string.split(sep)))

class Command:
    def __init__(self):
        self.reset()

    def __init__(self, str):
        self.reset()
        for s in str_split(str):
            self.args.append(s)

    def __str__(self):
        return str(self.args)

    def debug(self):
        print(self)

    def assemble(self):
        self.cmd = strcat(*self.args, sep=' ')

    def append(self, str):
        self.args.append(str)

    def reset(self):
        self.args = []

    def exec_async(self):
        self.assemble()
        print(self.cmd)

        p = Popen(self.args)

    def exec_sync(self):
        self.assemble()
        print(self.cmd)

        p = Popen(self.args)
        p.wait()

def strcat(*args, sep=''):
    return sep.join(args)

def prefix_set(pref, args):
    return list(map(lambda arg: strcat(pref, arg), args))

def suffix_set(args, suff):
    return list(map(lambda arg: strcat(arg, suff), args))

def pref_suff_set(args, pref='', suff=''):
    return list(map(lambda arg: strcat(pref, arg, suff), args))

def basename(name):
    pass
