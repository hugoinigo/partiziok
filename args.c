#include "args.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <getopt.h>
#include "util.h"
#include <string.h>

#include "gen.h"
#include "ff.h"
#include "mc.h"
#include "fr.h"
#include "nc.h"

static struct option options[] = {
        { "dims",           1, 0, 'n', },
                
        { "parts",          1, 0, 'p', },
        { "size-alpha",     1, 0,  0, },
        { "size-beta",      1, 0,  0, },
                
        { "max-time",       1, 0, 't', },
        { "time-alpha",     1, 0,  0, },
        { "time-beta",      1, 0,  0, }, 
                
        { "seed",           1, 0, 's', },
        { "part-gen",       1, 0, 'g', },
        { "max-size-job",   1, 0, 'm', },
        { "speed-factor",   1, 0,   0, },
        { "rotate",         1, 0,   0, },
        { "mode",           1, 0,   0, },
        { "criterion",      1, 0, 'c', },
        { "verbose",        0, 0, 'v', },
	{ "log",            0, 0, 'l', },
        { "version",        0, 0, 'V', },
        { "help",           0, 0, 'h', },
        { 0, 0, 0, 0, },
};

static char *opt_desc[] = {
        "number of dimensions of the network",
        "number of partitions to be generated",
        "alpha parameter for BETA distribution to generate the partition size",
        "beta parameter for BETA distribution to generate the partition size",
        "the maximum execution time partitions can be generated with",
        "alpha parameter for BETA distribution to generate the partition time",
        "beta parameter for BETA distribution to generate the partition time",
        "unique seed for the execution",
        "politic used to generate partitions",
        "maximum size of a partition (job)",
        "simulated speed-up for runtime",
        "rotate the partition if allocation fails",
        "select mode between normal or Monte Carlo simulation",
        "criterion for FR mode: none, dbl, bf, wf, ll, own (default is none)",
        "enable verbose output",
	"enable execution log",
        "print this help message",
        "print the current version of the program",
};

static bool opt_mask[OPTIONS_SIZE] = { 0 };

int parse_gen_part(const char *arg, struct config *cfg)
{
        int err = 0;

        if (strcmp(arg, "max-dim") == 0) {
                cfg->gen_job_policy = gen_max_dim_policy;
                strcpy(cfg->part_policy, "max-dim");
        } else if (strcmp(arg, "min-dim") == 0) {
                cfg->gen_job_policy = gen_min_dim_policy;
                strcpy(cfg->part_policy, "min-dim");
        } else {
                err = 1;
        }
        return(err);
}

int parse_mode(const char *arg, struct config *cfg)
{
        int err = 0;
        cfg->sched_policy = arg;
        /* first fit */
        if (strcmp(arg, "sim") == 0) {
                cfg->simulation = first_fit;
                cfg->sched_policy = "ff";
	} else if (strcmp(arg, "ff") == 0) {
		cfg->simulation = first_fit;
                /* free regions */
	} else if (strcmp(arg, "fr") == 0) {
		cfg->simulation = free_regions;
                /* monte carlo simulations */
        } else if (strcmp(arg, "mc") == 0) {
                cfg->simulation = monte_carlo;
                /* non contiguous */
        } else if (strcmp(arg, "nc") == 0) {
                cfg->simulation = non_contiguous;
	} else if (strcmp(arg, "ji") == 0) {
                cfg->simulation = job_info;
        } else {
                err = 1;
        }
        return err;
}

int parse_criterion(const char *arg, struct config *cfg)
{
        int err = 0;
        cfg->fr_crit_desc = arg;
        if (strcmp(arg, "none") == 0) {
                cfg->fr_criterion = 0;
        } else if (strcmp(arg, "dbl") == 0) {
                cfg->fr_criterion = 1;
        } else if (strcmp(arg, "bf") == 0) {
                cfg->fr_criterion = 2;
        } else if (strcmp(arg, "wf") == 0) {
                cfg->fr_criterion = 3;
        } else if (strcmp(arg, "ll") == 0) {
                cfg->fr_criterion = 4;
        } else if (strcmp(arg, "own") == 0) {
                cfg->fr_criterion = 5;
        } else {
                err = 1;
        }
        return err;
}

void print_option(unsigned long i)
{
        printf("  %c%c%c ",
               options[i].val ? '-' : ' ',
               options[i].val ? options[i].val : ' ',
               options[i].val ? ',' : ' ');
        printf("--%s%s%.*s%s\n",
               options[i].name,
               options[i].has_arg ? "=NUM" : "    ",
               (int) (COLUMN_FILL - strlen(options[i].name)),
               "                                                 ",
               opt_desc[i]);
}

void print_help(const char *pname)
{
        unsigned i;
        printf("Usage: %s [OPTIONS]... [NUMS]...\n", pname);

        printf("Execute a simulation of a network with the given options.\n");
        printf("OPTIONS:\n");

        for (i = 0; i < OPTIONS_SIZE; ++i) {
                print_option(i);
        }
}

void init_config(struct config *c)
{
        c->verbose = false;
	c->log = false;
        c->fr_criterion = FR_NONE;
        c->k = NULL;
}

int check_mandatory_args(const char *pname)
{
        long unsigned i;
        int result = 0;
        
        for (i = 0; i < MANDATORY_ARGS; ++i) {
                if (!opt_mask[i]) {
                        if (!result) {
                                printf("missing mandatory arguments:\n");
                        }
                        print_option(i);
                        result = -1;         
                }
        }
        if (result) {
                printf("type '%s --%s' for more information.\n", pname, options[OPTIONS_SIZE-1].name);
        }

        return result;
}

void print_config(const struct config *cfg)
{
        long unsigned i;
        printf("verbose        -> %s\n", cfg->verbose ? "true" : "false");
        printf("n              -> %lu\n", cfg->n);
        printf("part_list_size -> %lu\n", cfg->part_list_size);
        printf("part_max_time  -> %lu\n", cfg->part_max_time);
        printf("part_a, part_b -> %lu, %lu\n", cfg->part_a, cfg->part_b);
        printf("part_a, part_b -> %lu, %lu\n", cfg->part_a, cfg->part_b);
        printf("seed           -> %lu\n", cfg->seed);
        printf("max_job_size   -> %lu\n", cfg->max_job_size);
        printf("speed_factor   -> %f\n", cfg->speed_factor);
        printf("rotate         -> %lu\n", cfg->rotate);
        printf("fr criterion   -> %s\n", cfg->fr_crit_desc);
        printf("k              -> %lu", cfg->k[0]);
        for (i = 1; i < cfg->n; ++i) {
                printf(", %lu", cfg->k[i]);
        }
        printf("\n");
}

int parse_args(struct config *cfg, int argc, char **argv)
{
        int options_ind;
        int c;
        long result;
        float result_f;
        long unsigned i = 0;

        init_config(cfg);
                
        while (1) {
                c = getopt_long(argc, argv, "n:p:t:s:m:g:c:vlVh", options, &options_ind);
                if (c == -1)
                        break;

                switch (c) {
                        
                case 0: /* alpha edo beta */
                        switch (options_ind) {
                        case SIZE_ALPHA_IND:
                                parse_arg(optarg, &result);
                                cfg->part_a = result;
                                break;
                        case SIZE_BETA_IND:
                                parse_arg(optarg, &result);
                                cfg->part_b = result;
                                break;
                        case TIME_ALPHA_IND:
                                parse_arg(optarg, &result);
                                cfg->time_a = result;
                                break;
                        case TIME_BETA_IND:
                                parse_arg(optarg, &result);
                                cfg->time_b = result;
                                break;
                        case ROTATE_IND:
                                parse_arg(optarg, &result);
                                cfg->rotate = result;
                                break;
                        case SPEED_FACTOR_IND:
                                parse_arg_f(optarg, &result_f);
                                cfg->speed_factor = result_f;
                                break;
                        case MODE_IND:
                                if (parse_mode(optarg, cfg)) {
                                        fprintf(stderr, "unknown simulation mode: %s\n", optarg);
                                        exit(0);
                                }
                                break;
                        default:
                                printf("?? unreachable: [0%x]\n", c);
                                break;
                        }
                        opt_mask[options_ind] = true;
                        break;
                        
                case 'n': /* dims */
                        parse_arg(optarg, &result);
                        cfg->n = result;
                        opt_mask[0] = true;
                        break;

                 case 'm': /* dims */
                        parse_arg(optarg, &result);
                        cfg->max_job_size = result;
                        opt_mask[9] = true;
                        break;
       
                case 'p': /* parts */
                        parse_arg(optarg, &result);
                        cfg->part_list_size = result;
                        opt_mask[1] = true;
                        break;
                        
                case 't': /* max-time */
                        parse_arg(optarg, &result);
                        cfg->part_max_time = result;
                        opt_mask[4] = true;
                        break;

                case 's': /* seed */
                        parse_arg(optarg, &result);
                        cfg->seed = result;
                        opt_mask[7] = true;
                        break;
                 
                case 'g': /* gen-part */
                        if (parse_gen_part(optarg, cfg)) {
                                fprintf(stderr, "unknown gen part policy: %s\n", optarg);
                                exit(0);
                        }
                        opt_mask[8] = true;
                        break;
                        
                case 'v': /* verbose */
                        cfg->verbose = true;
                        break;

		case 'l':
			cfg->log = true;
			break;
                        
                case 'h':
                        print_help(argv[0]);
                        exit(0);
                        break;
                        
                case 'V': /* version */
                        printf("%s First Fit - 0.1\n", argv[0]);
                        exit(0);
                        break;

                case 'c': /* criterion */
                        if (parse_criterion(optarg, cfg)) {
                                fprintf(stderr, "unknown FR criterion: %s\n", optarg);
                                exit(0);
                        }
                        break;
                default:
                        printf("?? getopt returned character code 0%d ??\n", c);
                        exit(1);
                }
        }

        if (check_mandatory_args(argv[0])) {
                exit(1);
        }

        if ((argc - optind) != cfg->n) {
                fprintf(stderr, "remaining arguments must form a vector of size %lu\n", cfg->n);
                exit(1);
        }

        cfg->k = malloc(sizeof(*(cfg->k)) * cfg->n);
        if (cfg->k == NULL) {
                perror("malloc");
                exit(1);
        }

        cfg->network_size = 1;
        
        while (optind < argc) {
                parse_arg(argv[optind++], &result);
                cfg->k[i++] = result;
                cfg->network_size *= result;
        }
 
        /* print_config(cfg); */
        return 0;
}
