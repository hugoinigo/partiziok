OUT=ff
CC=gcc
HFILES=edges.h gen.h graph.h part.h util.h permutation.h args.h metric.h mc.h ff.h fr.h region.h list.h array.h nc.h log.h
CFILES=edges.c gen.c graph.c part.c util.c permutation.c args.c metric.c mc.c ff.c main.c fr.c region.c list.c array.c nc.c log.c
CFLAGS=-Wall -Wpedantic
SFLAGS=-O3

all:
	$(CC) $(CFLAGS) $(HFILES) $(CFILES) -lgsl -lgslcblas -lm -o $(OUT)

static:
	$(CC) $(CFLAGS) $(SFLAGS) -static -static-libgcc $(HFILES) $(CFILES) -lgsl -lgslcblas -lm -o $(OUT)

debug:
	$(CC) $(HFILES) $(CFILES) -lgsl -lgslcblas -lm -g -o $(OUT)

clean:
	rm $(OUT)
