#include "array.h"

long array_sum(const long *vec, unsigned len)
{
	unsigned i;
	long aux = 0;
	for (i = 0; i < len; ++i) {
		aux += vec[i];
	}
	return aux;
}

long array_prod(const long *vec, unsigned len)
{
	long i;
	long aux = 1;

	for (i = 0; i < len; ++i) {
		aux *= vec[i];
	}

	return aux;
}

void array_set(long *vec,
	       unsigned len,
	       long num)
{
	unsigned i;
	for (i = 0; i < len; ++i) {
		vec[i] = num;
	}
}

bool array_all(const long *vec,
	       unsigned len,
	       long eq)
{
	unsigned i;
	for (i = 0; i < len; ++i) {
		if (vec[i] > eq) {
			return false;
		}
	}
	return true;
}

bool array_any(const long *vec,
	       unsigned len,
	       long eq)
{
	unsigned i;
	for (i = 0; i < len; ++i) {
		if (vec[i] <= eq) {
			return true;
		}
	}
	return false;
}

void array_print(const long *vec, unsigned len)
{
        unsigned i;
        printf("[ ");
        for (i = 0; i < len; ++i) {
                printf("%ld ", vec[i]);
        }
        printf("]");
}

void array_fprint(FILE *stream,
		  const long *vec,
		  unsigned len)
{	
        unsigned i;
        printf("[ ");
        for (i = 0; i < len; ++i) {
                fprintf(stream, "%ld ", vec[i]);
        }
        printf("]");
}

