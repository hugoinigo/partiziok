#ifndef UTIL_H_
#define UTIL_H_

#include <stdbool.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define FREE 0
#define BIG_NUM 0xffffffff

#define uint unsigned int
#define ulong unsigned long

#define FR_NONE 0
#define FR_DBL 1
#define FR_BF 2
#define FR_WF 3
#define FR_LL 4
#define FR_OWN 5

struct config {
        int verbose;
	bool log;
        ulong n;
        ulong part_list_size;
        ulong part_max_time;
        ulong part_a, part_b;
        ulong time_a, time_b;
        ulong seed;
        void (*gen_job_policy) (long *vec,
                                const unsigned long *max_dim_size,
                                const struct config *c,
                                long job_size);

        char part_policy[50];
        const char *sched_policy;
 
        long *k;
        bool *dim_mask;
        unsigned long network_size;
        unsigned long max_job_size;
        unsigned long rotate;
        float speed_factor;
        int (*simulation) (const struct config *cfg);
        const char *fr_crit_desc;
        int fr_criterion;
};

void parse_arg(const char *arg, long *result);

void parse_arg_f(const char *arg, float *result);

void parse_params(const char *const *argv, struct config *c);

char *gen_filename(const struct config *c);

/* Iterate the given coords over the given partition.
 */
void iter_coords(long *coords,
                 const long *max_ind,
                 size_t n);

/*
bool all(const unsigned long *vec,
	 const unsigned long len,
	 const unsigned long num);

bool any(const unsigned long *vec,
	 const unsigned long len,
	 const unsigned long num);
*/

unsigned long part_count_edges(const long *k,
                               size_t n);

unsigned int root(unsigned int input, unsigned int n);

long mul(long *vec, unsigned long n);
#endif /* UTIL_H_ */
