#ifndef GEN_H_
#define GEN_H_

#include "util.h"

long gen_num_beta(const gsl_rng *r,
                  long max_dim,
                  long alpha,
                  long beta);

long gen_num_unif(const gsl_rng *r,
                  long min,
                  long max);

void printl(const unsigned long *vec, unsigned long n);

void printp(unsigned long **pl,
            unsigned long s,
            unsigned long n);

long **gen_job_list(const struct config *cfg);

unsigned long *max_dim(const struct config *cfg);

void gen_min_dim_policy(long *vec,
                        const unsigned long *max_dim_size,
                        const struct config *c,
                        long job_size);

void gen_max_dim_policy(long *vec,
                        const unsigned long *max_dim_size,
                        const struct config *c,
                        long job_size);

void gen_rnd_partition(long *vec,
                       const gsl_rng *r1,
                       const gsl_rng *r2,
                       const struct config *c);

void gen_free_job_list(long **part_list, unsigned long size);

#endif /* GEN_H_ */
