#include "graph.h"
#include "log.h"
#include "region.h"
#include "gen.h"
#include "permutation.h"
#include "list.h"
#include "array.h"
#include "metric.h"
#include "edges.h"
#define HG_VEC_IMPLEMENTATION
#include "vec.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <time.h>
#include <float.h>

static double total_alloc = 0;
static double total_dealloc = 0;

void printr(const struct reg *r)
{
	printf("c");
	array_print(r->coords, r->n);
	printf(" d");
	array_print(r->dims, r->n);
	printf(" %d", r->time);
}

void printrl(const struct list *l)
{
	struct node *i;
	struct reg *r;
	printf("{\n");
	for (i = l->first; i != NULL; i = i->next) {
		r = i->item;
		printf("  ");
		printr(r);
		printf(",\n");
	}
	printf("}\n");
}

bool job_fits(const long *job_dims,
              const long *mesh_dims,
              size_t n)
{
        size_t i;
        for (i = 0; i < n; ++i) {
                if (job_dims[i] > mesh_dims[i]) {
                        return false;
                }
        }
        return true;
}

int reg_cmp_dbl(const void *fst,
                const void *snd,
                void *arg)
{
	const struct reg *a = fst;
	const struct reg *b = snd;
	size_t i;
	int aux;
#ifdef ASSERT_DEFINED
	assert(a->n == b->n);
#endif

	for (i = a->n-1; i >= 0; i--) {
		aux = a->coords[i] - b->coords[i];
		if (aux != 0) {
			return aux;
		}
	}
	return 0;
}

int reg_cmp_bf(const void *fst,
               const void *snd,
               void *arg)
{
        const struct reg *a = fst;
        const struct reg *b = snd;

        long a_dims = array_prod(a->dims, a->n);
        long b_dims = array_prod(b->dims, b->n);
#ifdef ASSERT_DEFINED
        assert(a->n == b->n);
#endif

        return b_dims - a_dims;
}

int reg_cmp_wf(const void *fst,
               const void *snd,
               void *arg)
{
        const struct reg *a = fst;
        const struct reg *b = snd;

        long a_dims = array_prod(a->dims, a->n);
        long b_dims = array_prod(b->dims, b->n);
#ifdef ASSERT_DEFINED
        assert(a->n == b->n);
#endif

        return a_dims - b_dims;
}

/* Compare regions based on the link loss utilization */
int reg_cmp_ll(const void *fst,
	       const void *snd,
	       void *arg)
{
	const struct reg *a = fst;
	const struct reg *b = snd;
	struct graph *g = arg;
#ifdef ASSERT_DEFINED
	assert(a->n == b->n);
#endif

	long a_ll = count_lost_edges_a(g, a->coords, a->dims, a->n);
	long b_ll = count_lost_edges_a(g, b->coords, b->dims, b->n);
	return a_ll - b_ll;
}

int reg_cmp_lr(const void *fst,
	       const void *snd,
	       void *arg)
{
	/* const struct reg *a = fst; */
	/* const struct reg *b = snd; */
#ifdef ASSERT_DEFINED
	assert(a->n == b->n);
#endif


	return 0;
}

/*
void sort_list(struct list *reg_list,
	       int (*criterion) (const void *, const void *))
{
	struct node *i;
	struct hg_vec aux;
	hg_vec_alloc(&aux, reg_list->size);
	for (i = reg_list->first; i != NULL; i = i->next) {
		hg_vec_push(&aux, i->item);
	}
	hg_vec_sort(&aux, criterion);
	for (i = reg_list->last; i != NULL; i = i->previous) {
		i->item = hg_vec_pop(&aux);
	}
	hg_vec_free(&aux);
}
*/

void sort_list_r(struct list *reg_list,
		 int (*criterion) (const void *, const void *, void *),
		 void *arg)
{
	struct node *i;
	struct hg_vec aux;
	hg_vec_alloc(&aux, reg_list->size);
	for (i = reg_list->first; i != NULL; i = i->next) {
		hg_vec_push(&aux, i->item);
	}
	hg_vec_sort_r(&aux, criterion, arg);
	for (i = reg_list->last; i != NULL; i = i->previous) {
		i->item = hg_vec_pop(&aux);
	}
	hg_vec_free(&aux);
}

struct reg *job_assign_try_nosort(struct list *l,
				  const long *job_dims,
				  size_t n,
                                  int (*criterion) (const void *, const void *, void *),
                                  void *arg)
{
        struct node *iter;
        struct reg *r;

        for (iter = l->first; iter != NULL; iter = iter->next) {
                r = iter->item;
                if (job_fits(job_dims, r->dims, n)) {
                        list_remove(l, iter);
                        return r;
                }
        }

        return NULL;
}

struct reg *job_assign_try(struct list *l,
			   const long *job_dims,
			   size_t n,
			   int (*criterion) (const void *, const void *, void *),
                           void *arg)
{
        struct node *iter;
        struct reg *r;

	sort_list_r(l, criterion, arg);
        for (iter = l->first; iter != NULL; iter = iter->next) {
                r = iter->item;
                if (job_fits(job_dims, r->dims, n)) {
                        list_remove(l, iter);
                        return r;
                }
        }
        return NULL;
}

void list_move_by(struct list *dest,
		  struct list *src,
		  const struct reg *job,
		  bool (*func) (const struct reg *,
				const struct reg *,
				int *))
{
	struct node *iter;
	struct reg *aux;
	for (iter = src->first; iter != NULL; iter = iter->next) {
		aux = iter->item;
		if (func(job, aux, &aux->time)) {
			list_remove(src, iter);
			list_insert(dest, aux);
		}
	}
}

void list_insert_by(struct list *dest,
		    const struct list *src,
		    const struct reg *job,
		    bool (*func) (const struct reg *,
				  const struct reg *,
				  int *))
{
	struct node *iter;
	struct reg *aux;

	/* foreach (src, aux) { */
	for (iter = src->first; iter != NULL; iter = iter->next) {
		aux = iter->item;
		if (func(job, aux, &aux->time)) {
			list_insert(dest, aux);
		}
	}
}

bool reg_overlap_aux(const struct reg *a,
		     const struct reg *b,
		     int *aux)
{
	return (reg_overlap(a, b));
}

static inline bool job_finished(const struct reg *job)
{
	return (job->time <= 0);
}

bool job_finished_aux(const struct reg *a,
		      const struct reg *b,
		      int *aux)
{
	return (job_finished(b));
}

void get_surrounding_regs(struct list *quads_list,
			  struct list *overlaps_list,
			  const struct reg *job)
{
        struct node *i, *j;
        struct reg **quads;
        struct reg intersec, *reg;
	
        size_t n_quads;
        size_t q_ind;
        bool discard;

        reg_fill(&intersec, job->n);

        for (i = overlaps_list->first; i != NULL; i = i->next) {
                reg = i->item;
                reg_intersection(job, reg, &intersec);
                
                n_quads = quadrants(reg, &intersec, &quads);
                
                reg_free(reg);
                list_remove(overlaps_list, i);
                for (q_ind = 0; q_ind < n_quads; ++q_ind) {
                        discard = false;
                        for (j = quads_list->first; j != NULL; j = j->next) {
                                if (reg_subreg(j->item, quads[q_ind])) {
                                        reg_free(j->item);
                                        list_remove(quads_list, j);
                                } else if (reg_subreg(quads[q_ind], j->item)) {
                                        discard = true;
                                }
                        }

                        if (discard) {
                                reg_free(quads[q_ind]);
                        } else {
                                list_insert(quads_list, quads[q_ind]);
                        }
                }
                free(quads);

        }
        assert(overlaps_list->size == 0 && "overlaps list is not empty");
	reg_empty(&intersec);
}

void job_allocation(const struct reg *job,
		    struct list *regs_list,
		    struct list *ovl_regs)
{
	struct list sorrounding_regs;
	list_move_by(ovl_regs, regs_list, job, reg_overlap_aux);
	list_move_by(ovl_regs, regs_list, job, reg_semi_adjacent);

	list_alloc(&sorrounding_regs, ovl_regs->size * 2*job->n);
	get_surrounding_regs(&sorrounding_regs, ovl_regs, job);

	list_merge(regs_list, &sorrounding_regs);
	list_free(&sorrounding_regs);
}

void fill_lists(struct list *adj_list,
		struct list *semi_adj_list,
		const struct list *reg_list,
		const struct reg *job,
		size_t dim)
{
	struct node *i;
	struct reg *r;
	int adj_dim;
	for (i = reg_list->first; i != NULL; i = i->next) {
		r = i->item;
		if (reg_semi_adjacent(job, r, &adj_dim) && (adj_dim == 2*dim || adj_dim == 2*dim + 1)) {
			if (reg_adjacent(job, r, &adj_dim) && (adj_dim == 2*dim || adj_dim == 2*dim + 1)) {
				list_insert(adj_list, r);
			} else {
				list_insert(semi_adj_list, r);
			}
		}
	}
}

void job_deallocation(const struct reg *job, struct list *rl)
{
	size_t i, vec_iter;
	bool discard;
	struct list reg_adj;
	struct list semi_adj;
	struct list reg_list;
	struct hg_vec new_regs;
	struct node *iter;

	struct reg *reg, *aux;
	const struct reg *r;

	list_alloc(&reg_list, rl->size);
	list_alloc(&reg_adj, rl->size);
	list_alloc(&semi_adj, rl->size);

	aux = reg_clone(job);
	hg_vec_alloc(&new_regs, job->n * 2);
	hg_vec_push(&new_regs, aux);

	list_insert_by(&reg_list, rl, job, reg_semi_adjacent);

	for (i = 0; i < job->n; i++) {
		for (vec_iter = 0; vec_iter < new_regs.length; vec_iter++) {
			reg = hg_vec_get(&new_regs, vec_iter);

			list_empty(&semi_adj);
			list_empty(&reg_adj);

			fill_lists(&reg_adj, &semi_adj, &reg_list, reg, i);
            
			for (iter = semi_adj.first; iter != NULL; iter = iter->next) {
				r = iter->item;
				if (r->time == 2*i || r->time == 2*i + 1) {
					aux = reg_clone(r);
					reg_intersection_dim(aux, reg, aux, i);
					reg_union_dim(aux, reg, aux, i);
					hg_vec_push(&new_regs, aux);
				}
			}

			for (iter = reg_adj.first; iter != NULL; iter = iter->next) {
				r = iter->item;
				if (r->time == 2*i || r->time == 2*i + 1) {
					reg_union_dim(reg, r, reg, i);
				}
			}
		}
	}

	// TODO: en vez de rl manejar reg_list que es mas pequeña
	for (vec_iter = 0; vec_iter < new_regs.length; vec_iter++) {
		discard = false;
		aux = hg_vec_get(&new_regs, vec_iter);
		for (iter = rl->first; iter != NULL; iter = iter->next) {
			if (reg_subreg(iter->item, aux)) {
				reg_free(iter->item);
				list_remove(rl, iter);
			} else if (reg_subreg(aux, iter->item)) {
				discard = true;
			}
		}

		if (discard) {
			reg_free(aux);
		} else {
			list_insert(rl, aux);
		}	
	}

	list_free(&reg_adj);
	list_free(&semi_adj);
	list_free(&reg_list);
	hg_vec_free(&new_regs);
}

int joblist_time_min(const struct list *job_list)
{
	int min = INT_MAX;
	const struct reg *job;
	struct node *iter;

	for (iter = job_list->first; iter != NULL; iter = iter->next) {
		job = iter->item;
		if (job->time < min) {
			min = job->time;
		}
	}

	return min;
}

void joblist_time_dec(struct list *job_list, int delta_time)
{
	struct node *iter;
	struct reg *job;

	for (iter = job_list->first; iter != NULL; iter = iter->next) {
		job = iter->item;
		job->time -= delta_time;
	}
}

static inline double cur_util_generic(int time,
				      int prev_time,
				      size_t alloc_elems,
				      size_t total_elems)
{
	return (time - prev_time) * alloc_elems / (double) total_elems;
}

static inline void node_util_update(struct graph *g)
{
	g->node_utilization += cur_util_generic(g->time, g->prev_time, g->alloc_nodes, g->len);
}

static inline void edge_util_update(struct graph *g)
{
	g->edge_utilization += cur_util_generic(g->time, g->prev_time, g->alloc_edges, g->e);
}

void link_loss_update(struct graph *g)
{
	long alloc_and_lost_edges;
	alloc_and_lost_edges = (g->lost_edges + g->alloc_edges);
	if (alloc_and_lost_edges != 0) {
		g->edge_loss += g->lost_edges / ((double) alloc_and_lost_edges);
	} else {
		g->edge_loss += g->lost_edges / 1.0;
	}
}

// TODO: move this to other place: "metric.h"
static void fprintm(FILE *restrict s,
                    const char *msg,
                    const struct metric *m)
{
        fprintf(s, "%s min: %lu\n", msg,
                m->min == ULONG_MAX ? 0 : m->min);
        fprintf(s, "%s max: %lu\n", msg, m->max);
        fprintf(s, "%s avg: %lf\n", msg, m->avg);
}

static void fprintmd(FILE *restrict s,
		     const char *msg,
		     const struct metricd *m)
{
        fprintf(s, "%s min: %lf\n", msg,
                m->min == DBL_MAX ? 0 : m->min);
        fprintf(s, "%s max: %lf\n", msg, m->max);
        fprintf(s, "%s avg: %lf\n", msg, m->avg);
}

static double time_delta(struct timespec prev_ts, struct timespec post_ts)
{
	double result = post_ts.tv_sec - prev_ts.tv_sec;
	result += (post_ts.tv_nsec - prev_ts.tv_nsec) / 1000000000.0;
	return result;
}

int free_regions(const struct config *cfg)
{
	struct graph g;

        struct list regions_list;
        struct list overlaps_list;

	struct list active_jobs;
	const size_t n = cfg->n;
	const long *k = cfg->k;

	long unsigned delta_time;
        double alloc_delta = 0, dealloc_delta = 0;

        struct reg *reg;
        struct reg *job;
	struct node *iter;

	long **job_list;
	size_t i;

	bool job_assigned;
	char *fname;
	FILE *f;
        FILE *log;

	long unsigned max_permu, pind;
	long *permu;

	unsigned long atrials = 0;
	struct timespec begin, end;

	if (cfg->log) {
		log = fopen("proba.txt", "w+");
                fprintf(log, "%ld;", n);
                log_array(log, k, n);
                fprintf(log,";%ld;\n", cfg->part_list_size*2);
	}


	job_list = gen_job_list(cfg);
	graph_alloc(&g, k, n);

	if (cfg->rotate) {
		max_permu = factorial(n);
	} else {
		max_permu = 1;
	}

	permu = malloc(sizeof(*permu) * n);
        list_alloc(&regions_list, cfg->part_list_size * n * 2);
        list_alloc(&overlaps_list, cfg->part_list_size * n * 2);
	list_alloc(&active_jobs, cfg->part_list_size);

	reg = reg_alloc(n);
        memcpy(reg->dims, k, sizeof(*(reg->dims)) * n);
        list_insert(&regions_list, reg);

        int (*crit_fn) (const void *, const void *, void *) = NULL;
        struct reg * (*job_assign_try_fn) (struct list *,
                                           const long *,
                                           size_t,
                                           int (*crit) (const void *, const void *, void *),
                                           void *) = job_assign_try;
        void *arg = NULL;

        switch (cfg->fr_criterion) {
        case FR_NONE:
                job_assign_try_fn = job_assign_try_nosort;
                printf("fr none\n");
                break;
        case FR_DBL:
                crit_fn = reg_cmp_dbl;
                printf("fr dbl\n");
                break;
        case FR_BF:
                crit_fn = reg_cmp_bf;
                printf("fr bf\n");
                break;
        case FR_WF:
                crit_fn = reg_cmp_wf;
                printf("fr wf\n");
                break;
        case FR_LL:
                crit_fn = reg_cmp_ll;
                arg = &g;
                printf("fr ll\n");
                break;
        case FR_OWN:
                crit_fn = reg_cmp_lr;
                arg = NULL;
                printf("fr own\n");
                break;
        }

	if (cfg->verbose) {
		printf("[VERBOSE] enabled\n");
	}
	for (i = 0; i < cfg->part_list_size; ++i) {
		g.prev_time = g.time;
		if (cfg->verbose) {
			printf("[ALLOC] job id: %ld", i+1);
			fflush(stdout);
		}

		for (pind = 0; pind < max_permu; pind++) {
                        lexicographical_permutation(permu, pind, job_list[i], n);

                        clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
			reg = job_assign_try_fn(&regions_list, permu, n, crit_fn, arg);
			clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                        alloc_delta += time_delta(begin, end);

			if (reg != NULL) {
				if (cfg->verbose) {
					printf("\tDONE (%f)\n", alloc_delta);
				}
				job_assigned = false;
				list_insert(&overlaps_list, reg);

				//////// funcion aparte o asi
				job = reg_alloc(n);
				job->id = i+1;
				reg_set(job, reg->coords, permu, n, job_list[i][n]);

				clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
				job_allocation(job, &regions_list, &overlaps_list);
				clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                                alloc_delta += time_delta(begin, end);
				///////

				set_graph(&g, job->coords, job->dims, job->id);

				g.time += 1;

				node_util_update(&g);
				edge_util_update(&g);
				link_loss_update(&g);

				update_metric(&afails_rot, pind);
				update_metric(&afails, atrials);
				atrials = 0;

				long job_node_count = array_prod(job->dims, job->n);

				update_metric(&psize, job_node_count);
				update_metric(&ptime, job->time);

				g.alloc_nodes += job_node_count;
				g.alloc_edges += part_count_edges(job->dims, job->n);
				g.lost_edges += count_lost_edges_a(&g, job->coords, job->dims, job->n);

				update_metric(&reg_list_size, regions_list.size);
				update_metricd(&alloc_time, alloc_delta);
                                total_alloc += alloc_delta;

                                alloc_delta = 0;

				list_insert(&active_jobs, job);
				joblist_time_dec(&active_jobs, 1);

				g.event += 1;
				if (cfg->log) {
					log_state(log, g.time, &active_jobs, &regions_list);
				}

				break;
			} else {
				// ITXARON BEHAR DA, EZ DA SARTZEN
				job_assigned = true;
			}
		}
		if (job_assigned) {
			if (cfg->verbose) {
				printf("\tWAITING (%d)\n", job->id);
			}
			i -= 1;
			delta_time = joblist_time_min(&active_jobs);
			joblist_time_dec(&active_jobs, delta_time);
			g.time += delta_time;
			atrials += 1;

			node_util_update(&g);
			edge_util_update(&g);

			update_metric(&pwait, delta_time);
		}
		for (iter = active_jobs.first; iter != NULL; iter = iter->next) {
			job = iter->item;
			if (job_finished(job)) {

				if (cfg->verbose) {
					printf("[DEALLOC] Job id: %d", job->id);
					fflush(stdout);
				}
				clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
				job_deallocation(job, &regions_list);
				clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                                dealloc_delta = time_delta(begin, end);
				if (cfg->verbose) {
					printf("\tDONE (%f)\n", dealloc_delta);
				}

				link_loss_update(&g);

				g.alloc_nodes -= array_prod(job->dims, job->n);
				g.alloc_edges -= part_count_edges(job->dims, job->n);
				g.lost_edges -= count_lost_edges_a(&g, job->coords, job->dims, job->n);

				update_metric(&reg_list_size, regions_list.size);
				update_metricd(&dealloc_time, dealloc_delta);
                                total_dealloc += dealloc_delta;

				set_graph(&g, job->coords, job->dims, FREE);
				g.event += 1;

				list_remove(&active_jobs, iter);
				reg_free(job);

				if (cfg->log) {
					log_state(log, g.time, &active_jobs, &regions_list);
				}

			}
		}
	}

	while (!list_isempty(&active_jobs)) {
		delta_time = joblist_time_min(&active_jobs);
		joblist_time_dec(&active_jobs, delta_time);

		g.prev_time = g.time;
		g.time += delta_time;

		node_util_update(&g);
		edge_util_update(&g);

		for (iter = active_jobs.first; iter != NULL; iter = iter->next) {
			job = iter->item;
			if (job_finished(job)) {
				if (cfg->verbose) {
					printf("[DEALLOC] job id: %d", job->id);
					fflush(stdout);
				}
				clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
				job_deallocation(job, &regions_list);
				clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                                dealloc_delta = time_delta(begin, end);
				if (cfg->verbose) {
					printf("\tDONE (%f)\n", dealloc_delta);
				}
				
				link_loss_update(&g);

				g.alloc_nodes -= array_prod(job->dims, job->n);
				g.alloc_edges -= part_count_edges(job->dims, job->n);
				g.lost_edges -= count_lost_edges_a(&g, job->coords, job->dims, job->n);

				update_metric(&reg_list_size, regions_list.size);
				update_metricd(&dealloc_time, dealloc_delta);
                                total_dealloc += dealloc_delta;

				set_graph(&g, job->coords, job->dims, FREE);
				g.event += 1;

				list_remove(&active_jobs, iter);
				reg_free(job);

				if (cfg->log) {
					log_state(log, g.time, &active_jobs, &regions_list);
				}
 				//printf("dealloc time: %luus\n", time_delta(prev_tv, post_tv));
			}
		}
	}
	fname = gen_filename(cfg);
	f = fopen(fname, "w+");

        double total_time = total_alloc + total_dealloc;
        alloc_time.avg = total_alloc / cfg->part_list_size;
	dealloc_time.avg = total_dealloc / cfg->part_list_size;

        printf("%s\n", fname);
	printf("node utilization:   %lf\n", g.node_utilization / g.time);
	printf("total alloc time:   %lf\n", total_alloc);
	printf("total dealloc time: %lf\n", total_dealloc);
	printf("total time:         %lf\n", total_time);
	printf("alloc time avg:     %lf\n", alloc_time.avg);
	printf("dealloc time avg:   %lf\n", dealloc_time.avg);

	fprintf(f, "%s\n", fname);
	fprintf(f, "node utilization: %lf\n", g.node_utilization / g.time);
        fprintf(f, "edge utilization: %lf\n", g.edge_utilization / g.time);
        fprintf(f, "part utilization: %lf\n", g.edge_loss / (2 * cfg->part_list_size));

        psize.avg /= cfg->part_list_size;
        fprintm(f, "part size", &psize);

        ifrag.avg /= cfg->part_list_size;
        fprintm(f, "real part size", &ifrag);

        ptime.avg /= cfg->part_list_size;
        fprintm(f, "part time", &ptime);

        pwait.avg /= cfg->part_list_size;
        fprintm(f, "part wait", &pwait);

        afails.avg /= cfg->part_list_size;
        fprintm(f, "alloc fails", &afails);

        afails_rot.avg /= cfg->part_list_size;
        fprintm(f, "alloc fails rot", &afails_rot);

        fprintf(f, "simulation time: %lu\n", g.time);

	fprintf(f, "alloc time: %lf\n", total_alloc);
	fprintmd(f, "alloc time", &alloc_time);

	fprintf(f, "dealloc time: %lf\n", total_dealloc);
	fprintmd(f, "dealloc time", &dealloc_time);

        fprintf(f, "total time: %lf\n", total_time);

	reg_list_size.avg /= cfg->part_list_size * 2;
	fprintm(f, "reg list size", &reg_list_size);

	for (iter = regions_list.first; iter != NULL; iter = iter->next) {
		reg_free(iter->item);
	}
	gen_free_job_list(job_list, cfg->part_list_size);
	list_free(&regions_list);
        list_free(&overlaps_list);
	list_free(&active_jobs);

	free(fname);
        fclose(f);
	if (cfg->verbose) {
		printf("asdf\n");
	}
	if (cfg->log) {
		fclose(log);
	}

        graph_free(&g);
        free(permu);

	return (0);
}

