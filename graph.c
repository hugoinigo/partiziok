#include "graph.h"
#include "util.h"
#include "edges.h"

#include <stdlib.h>
#include <string.h>

/* Initialize the given graph with the given parameters:
 * n dimensions and k length for each dimension.
 */
void graph_alloc(struct graph *g,
                 const long *k,
                 size_t n)
{
        unsigned long *data;
        unsigned long i, len;

        edge_mem_alloc(n);

        /* We won't check the errors */
        /* len = pow(k, n); */
        len = 1;
        for (unsigned i = 0; i < n; ++i) {
                len *= k[i];
        }

        data = malloc(sizeof(unsigned long) * len);
        if (data == NULL) {
                perror("graph_alloc");
                exit(2);
        }
	/* memset(data, 0, sizeof(unsigned long) * len); */
        for (i = 0; i < len; ++i) {
                data[i] = FREE;
        }

        g->data = data;
        g->len = len;
        g->n = n;
        g->e = part_count_edges(k, n);
        g->k = k;
        g->node_utilization = 0;
        g->edge_utilization = 0;
        g->edge_loss = 0;
        g->time = 0;
        g->prev_time = 0;
        g->lost_edges = 0;
        g->alloc_nodes = 0;
        g->alloc_edges = 0;
        g->event = 0;
}

/* Get the graph element at the given coords and
 * an optional offset, if offset is NULL it will be
 * ommited.
 */
unsigned long *graph_elem(const struct graph *g,
                          const long *offset,
                          const long *coords)
{
        unsigned long ind = coords[0];
        unsigned long i;

        if (offset == NULL) {
                for (i = 1; i < g->n; ++i) {
                        ind = ind * g->k[i] + coords[i];
                }
        } else {
                ind += offset[0];
                for (i = 1; i < g->n; ++i) {
                        ind = ind * g->k[i] + coords[i] + offset[i];
                }
        }

        return g->data + ind;
}

/* Free all the resources from the graph.
 */
void graph_free(struct graph *g)
{
        edge_mem_free();
        free(g->data);
}

/* Set a graph's region to the given state. The region
 * is given by the initial coords and the partition.
 */
void set_graph(struct graph *g,
               const long *coords,
               const long *part,
               unsigned long id)
{
        unsigned long res;
        unsigned long *item;
        long *iter = calloc(sizeof(long), g->n);
        size_t i;

        res = 1;
        for (i = 0; i < g->n; i++) {
                res *= part[i];
        }

        for (i = 0; i < res; i++) {
                item = graph_elem(g, coords, iter);
                *item = id;

                iter_coords(iter, part, g->n);
        }

        free(iter);
}

void fprintg(FILE *restrict s, const struct graph *g)
{
        unsigned long i;
        fprintf(s, "(%lu) ", g->event);
        for (i = 0; i < g->len; ++i) {
                fprintf(s, "%lu ", g->data[i]);
        }
        /* fprintf(s, "u %lf\n", g->utilization / g->time); */
        /* fprintf(s, "e %lu ", g->e); */
        fprintf(s, "nu %lf eu %lf lu %lf t %lu ",
                g->node_utilization,
                g->edge_utilization,
                g->edge_loss,
                g->time);
}
