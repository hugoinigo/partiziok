#include "edges.h"
#include "util.h"

#include <string.h>
#ifdef ASSERT_DEFINED
#include <assert.h>
#endif // ASSERT_DEFINED
#include <malloc.h>
#include <stdbool.h>

static long *coords_aux;
static long *dims_aux;
static long *iter;

void edge_mem_alloc(size_t n)
{
        coords_aux = malloc(sizeof(*coords_aux) * n);
        dims_aux = malloc(sizeof(*dims_aux) * n);
        iter = calloc(n, sizeof(*iter));
}

void edge_mem_free()
{
        free(coords_aux);
        free(dims_aux);
        free(iter);
}

/* Iterate the given coords over the given partition.
 */
void iter_coords_omit(long *coords,
                      const long *max_ind,
                      size_t n,
                      size_t omit)
{
        size_t i;
        for (i = 0; i < n; ++i) {
                if (i == omit) {
                        continue;
                }
                if (coords[i] + 1 < max_ind[i]) {
                        coords[i] += 1;
                        break;
                }
                coords[i] = 0;
        }
}

bool all_zeros_omit(const long *vec,
                    size_t n,
                    size_t omit)
{
        unsigned long i;
        for (i = 0; i < n; ++i) {
                if (i == omit)
                        continue;
                if (vec[i] != 0)
                        return false;
        }
        return true;
}

/*
unsigned long count_part_nodes(const unsigned long *dims, unsigned long n)
{
        // Aurrebaldintza modura, partizioaren dimentsio
        // guztiak > 0 izango dira
        unsigned long count = 1;
        unsigned long i;
        for (i = 0; i < n; ++i) {
                count *= dims[i];
        }
        return count;
}
*/

unsigned long count_adjacent_edges(const struct graph *g,
                                   const long *dims,
                                   const long *coords,
                                   const int omit)
{
        //unsigned long *iter = calloc(sizeof(unsigned long), g->n);
        memset(iter, 0, sizeof(*iter));
        unsigned long count = 0;

        do {
                if (*graph_elem(g, coords, iter) == FREE) {
                        count++;
                }
                iter_coords_omit(iter, dims, g->n, omit);
        } while (!all_zeros_omit(iter, g->n, omit));
        /*
        if (count != 0) {
                printf("dims: ");
                for (int i = 0; i < g->n; ++i) {
                        printf("%lu ", dims[i]);
                }
                printf("\ncoords: ");
                for (int i = 0; i < g->n; ++i) {
                        printf("%lu ", coords[i]);
                }
                printf("\n");
        }
        */

        //free(iter);

        return count;
}

unsigned long count_lost_edges(const struct part *p, const struct graph *g)
{
        unsigned i;
        unsigned long init_coord, init_dim;

        const unsigned long n = g->n;
        unsigned long lost_nodes = 0;

#ifdef ASSERT_DEFINED
        assert(p != NULL && "provided partition is null");
        assert(p->coords != NULL && "provided coords is null");
        assert(p->dimensions != NULL && "provided dimensions is null");
#endif

        memcpy(coords_aux, p->coords, sizeof(*coords_aux) * n);
        memcpy(dims_aux, p->dimensions, sizeof(*dims_aux) * n);

        for (i = 0; i < n; ++i) {
                init_coord = coords_aux[i];
                init_dim = dims_aux[i];

                dims_aux[i] = 1;

                /* lehenengo kasua */
                if (coords_aux[i] != 0) {
                        coords_aux[i] -= 1;
                        lost_nodes += count_adjacent_edges(g, dims_aux, coords_aux, i);
                }

                /* bigarren kasua */
                coords_aux[i] = init_dim + init_coord;
                if (coords_aux[i] < g->k[i]) {
                        lost_nodes += count_adjacent_edges(g, dims_aux, coords_aux, i);
                }

                coords_aux[i] = init_coord;
                dims_aux[i] = init_dim;
        }

        return lost_nodes;
}

long count_lost_edges_a(const struct graph *g,
			const long *coords,
			const long *dims,
			size_t n)
{
	size_t i;
	long init_coord, init_dim;

	long lost_edges = 0;

#ifdef ASSERT_DEFINED
        assert(coords != NULL);
        assert(dims != NULL);
#endif

	memcpy(coords_aux, coords, sizeof(*coords) * n);
	memcpy(dims_aux, dims, sizeof(*dims) * n);

	for (i = 0; i < n; i++) {
		init_coord = coords_aux[i];
		init_dim = dims_aux[i];

		dims_aux[i] = 1;
		if (coords_aux[i] != 0) {
			coords_aux[i] -= 1;
			lost_edges += count_adjacent_edges(g, dims_aux, coords_aux, i);
		}
		coords_aux[i] = init_dim + init_coord;
		if (coords_aux[i] < g->k[i]) {
			lost_edges += count_adjacent_edges(g, dims_aux, coords_aux, i);
		}

		coords_aux[i] = init_coord;
		dims_aux[i] = init_dim;
	}

	return (lost_edges);
}

