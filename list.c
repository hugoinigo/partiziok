#include "list.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define FREE 0
#define ALLOC !FREE

void nodes_reset(struct node *nodes, size_t len)
{
	memset(nodes, 0, sizeof(struct node) * len);
	/*
        size_t i;
        for (i = 0; i < len; ++i) {
                nodes[i].status = FREE;
		nodes[i].previous = NULL;
		nodes[i].next = NULL;
        }
	*/
}

int list_alloc(struct list *list, size_t cap)
{
        struct node *data = malloc( cap * sizeof(struct node));
        if (data == NULL) {
                return -1;
        }

        nodes_reset(data, cap);
        list->data = data;

        list->last = NULL;
        list->first = NULL;

        list->capacity = cap;
        list->size = 0;
        list->next_free = 0;

        return 0;
}

void list_empty(struct list *list)
{
	nodes_reset(list->data, list->capacity);
	list->last = NULL;
	list->first = NULL;

	list->size = 0;
	list->next_free = 0;
}

void list_free(struct list *list)
{
        free(list->data);
}

/* TAKES REFERENCE of the given struct and inserts it in the list
 * if there is no free space the item wont be inserted and
 * -1 will be returned.
 */
int list_insert(struct list *list, const void *item)
{
        struct node *n;
        size_t i;

        assert(item != NULL && "The given node is NULL");
        assert(list != NULL && "The given list is NULL");

        /* The list is full, return with error */
        if (list->size >= list->capacity) {
                return -1;
        }

        n = &list->data[list->next_free];

        /* If the list is empty set this node as the first */
        if (list->size == 0) {
                list->first = n;
        } else {
                list->last->next = n;
        }

        n->next = NULL;
        n->previous = list->last;
        n->status = ALLOC;

        n->item = (void *) item;

        list->last = n;

        /* Find fordward the next free node */
        for (i = list->next_free; i < list->capacity; ++i) {
                if (list->data[i].status == FREE) {
                        break;
                }
        }
        list->next_free = i;
        list->size += 1;

        return 0;
}

void list_remove(struct list *list, struct node *node)
{
        assert(node != NULL && "The given node is NULL");
        assert(list != NULL && "The given list is NULL");
        assert(list->size != 0 && "The given list is empty");

        if (node->previous != NULL) {
                node->previous->next = node->next;
        }
        if (node->next != NULL) {
                node->next->previous = node->previous;
        }
        if (list->first == node) {
                list->first = node->next;
        }
        if (list->last == node) {
                list->last = node->previous;
        }
        // node->next = NULL;
        // node->previous = NULL;
        node->status = FREE;

        if ((list->data + list->next_free) > node) {
                list->next_free = node - list->data;
        }
        list->size -= 1;
}

struct node *list_node(struct list *list, const void *item)
{
        struct node *iter = list->first;
        while (iter != NULL && iter->item != item) {
                iter = iter->next;
        }

        return iter;
}

void list_merge(struct list *dest, const struct list *src)
{
        assert(dest->capacity - dest->size >= src->size && "Source list does not fit in dest");
        struct node *i;
        for (i = src->first; i != NULL; i = i->next) {
		list_insert(dest, i->item);
        }
}

int list_isempty(const struct list *list)
{
	return (list->size == 0);
}
