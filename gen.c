#include "gen.h"
#include "metric.h"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <stdlib.h>

long gen_num_beta(const gsl_rng *r,
                  long scale,
                  long alpha,
                  long beta)
{
        long result = gsl_ran_beta(r, alpha, beta) * scale;
        if (result <= 1)
                result = 2;
        return result;
}

long gen_num_unif(const gsl_rng *r,
                  long min,
                  long max)
{
        long result = gsl_ran_flat(r, min, max);
        return(result);
}  

unsigned choose_dim(const bool *dim_mask, unsigned n)
{
        unsigned i = rand() % n;
        while (!dim_mask[i]) {
                i = (i + 1) % n;
        }
        return i + 1;
}

unsigned long *max_dim( const struct config *c){
    
        unsigned long i, j, aux;
        unsigned long *max_dim = calloc(c->n, sizeof(unsigned long));
        unsigned long *max_dim_aux = calloc(c->n, sizeof(unsigned long));
        unsigned long *max_dim_size = calloc(c->n-1, sizeof(unsigned long));

        aux = 0;
        for(i = 0; i < c->n; i++){
            for(j = 0; j < c->n; j++){
                if(max_dim_aux[j] == 0 && (c->k[j] > max_dim[i])){
                    max_dim[i] = c->k[j];
                    aux = j;
                }
            }
            max_dim_aux[aux] = 1;
        }

        max_dim_size[0] = max_dim[0] * max_dim[1];
        for(i = 1; i < c->n - 1; i++){
            max_dim_size[i] = max_dim_size[i - 1] * max_dim[i + 1]; 
        }

        //printl(max_dim_size, c->n-2);
        free(max_dim_aux);
        free(max_dim);
        return(max_dim_size);
}

void gen_min_dim_policy(long *vec,
                        const unsigned long *max_dim_size,
                        const struct config *c,
                        long job_size)
{
        unsigned long i, j, aux;
        unsigned int length, length_aux;

        /* size = gen_num_beta(r, c->max_job_size, c->part_a, c->part_b); */
        
        for (i = 0; i < c->n; ++i) {
                vec[i] = 1;
        }

        for (i = 0; i < c->n - 1; ++i) {
            if(job_size <= max_dim_size[i]){
                break;
            }
        }

        length = root(job_size, i + 2);
        
        j = 0;
        aux = 0;
        while (j < c->n) {
            if(c->k[j] >= length){
                vec[j] = length;
                aux++;
            }
            if(aux == (i + 2))
                break;
            j++;
        }

        length_aux = mul(vec, c->n);
        j = 0;
        while(length_aux < job_size){
            if(vec[j] < c->k[j]){
                vec[j]++;
                j = (j + 1) % (i + 2);
                length_aux = mul(vec, c->n);
            }
            else{
                j = (j + 1) % (i + 2);
            }

        }
        update_metric(&ifrag, job_size);
}

void gen_max_dim_policy(long *vec,
                        const unsigned long *max_dim_size,
                        const struct config *c,
                        long job_size)
{
        unsigned long i, j, aux;
        unsigned int length, length_aux;

        /* size = gen_num_beta(r, c->max_job_size, c->part_a, c->part_b); */
        
        for (i = 0; i < c->n; ++i) {
                vec[i] = 1;
        }

        length = root(job_size, c->n);
        j = 0;
        aux = 0;
        while (j < c->n) {
            if(c->k[j] >= length){
                vec[j] = length;
                aux++;
            }
            if(aux == c->n)
                break;
            j++;
        }

        length_aux = mul(vec, c->n);
        j = 0;
        while(length_aux < job_size){
            if(vec[j] < c->k[j]){
                vec[j]++;
                length_aux = mul(vec, c->n);
            }
            j = (j + 1) % c->n;
        }
        update_metric(&ifrag, job_size);
}

void gen_rnd_partition(long *vec,
                       const gsl_rng *r1,
                       const gsl_rng *r2,
                       const struct config *c)
{

        unsigned long i, size, size_aux;
        long idx;

        for (i = 0; i < c->n; ++i) {
                vec[i] = 1;
        }

        size = gen_num_unif(r1, 1, c->network_size + 1);

        size_aux = mul(vec, c->n);
        while (size_aux < size) {
                idx = gen_num_unif(r2, 0, c->n);
                if (c->k[idx] >= (vec[idx] + 1)) {
                        vec[idx]++;
                        size_aux = mul(vec, c->n);
                }
        }
        vec[c->n] = gen_num_beta(r2, c->part_max_time, c->time_a, c->time_b);
        update_metric(&ifrag, size);
}


void printl(const unsigned long *vec, unsigned long n)
{
        unsigned i;
        for (i = 0; i <= n; ++i) {
                printf("%li ", vec[i]);
        }
        printf("\n");
}

void printp(unsigned long **pl,
            unsigned long s,
            unsigned long n)
{
        unsigned long i;
        for (i = 0; i < s; ++i) {
                printl(pl[i], n);
        }
}

long **gen_job_list(const struct config *cfg)
{
        long **part_list, *vec, size;
        long unsigned *max_dim_size;
        gsl_rng *r;
        unsigned long i;

        part_list = malloc(sizeof(long *) * cfg->part_list_size);
        if (part_list == NULL) {
                perror("malloc");
                return NULL;
        }

        r = gsl_rng_alloc(gsl_rng_taus);
        gsl_rng_set(r, cfg->seed + 1);
        srand(cfg->seed);

        max_dim_size = max_dim(cfg);

        for (i = 0; i < cfg->part_list_size; ++i) {
                vec = malloc(sizeof(cfg->k) * (cfg->n+1));
                size = gen_num_beta(r, cfg->max_job_size, cfg->part_a, cfg->part_b);
                cfg->gen_job_policy(vec, max_dim_size, cfg, size);
                vec[cfg->n] = gen_num_beta(r, cfg->part_max_time, cfg->time_a, cfg->time_b)* cfg->speed_factor;
                part_list[i] = vec;
        }
        gsl_rng_free(r);
        free(max_dim_size);
        return part_list;
}

void gen_free_job_list(long **part_list, unsigned long size)
{
        unsigned long i;
        for (i = 0; i < size; ++i) {
                free(part_list[i]);
        }

        free(part_list);
}
