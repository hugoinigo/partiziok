#ifndef __FREE_REGIONS_H
#define __FREE_REGIONS_H

#include "util.h"

int free_regions(const struct config *cfg);

#endif // __FREE_REGIONS_H
