#ifndef LIST_H_
#define LIST_H_

#include <stdlib.h>

#define LIST_INITIAL_CAP 128
#define foreach(list, elem) for (struct node *__iter = (list)->first; __iter != NULL; __iter = __iter->next, (elem) = __iter->item)

struct node {
        void *item;
        struct node *next;
        struct node *previous;
        int status;
};

struct list {
        struct node *data;
        struct node *first;
        struct node *last;
        size_t capacity;
        size_t size;
        size_t next_free;
};

int list_alloc(struct list *list, size_t cap);
void list_free(struct list *list);
int list_insert(struct list *list, const void *item);
void list_remove(struct list *list, struct node *node);
struct node *get_node(struct list *list, const void *item);
void list_merge(struct list *dest, const struct list *src);
void list_empty(struct list *list);
int list_isempty(const struct list *list);

#endif /* LIST_H_ */
