#include "gen.h"
#include "graph.h"
#include "metric.h"
#include "list.h"

#include <limits.h>
#include <sys/time.h>

static long unsigned total_alloc = 0;
static long unsigned total_dealloc = 0;

bool try_fit(struct graph *g,
             long job,
             unsigned long id)
{
        long job_rem_nodes = job;
        if ((g->len - g->alloc_nodes) < job_rem_nodes) {
                return false;
        }

        for (unsigned long i = 0; i < g->len; ++i) {
                if (job_rem_nodes == 0) {
                        break;
                }
                if (g->data[i] == FREE) {
                        g->data[i] = id;
                        job_rem_nodes--;
                }
        }
        return true;
}

void dealloc_job(struct graph *g,
                 long job,
                 unsigned long id)
{
        long job_rem_nodes = job;
        for (unsigned long i = 0; i < g->len; ++i) {
                if (job_rem_nodes == 0) {
                        break;
                }
                if (g->data[i] == id) {
                        g->data[i] = FREE;
                        job_rem_nodes--;
                }
        }
}

long **gen_job_nodes(const struct config *c)
{
        gsl_rng *r = gsl_rng_alloc(gsl_rng_taus);
        long **result = malloc(sizeof(unsigned long *) * c->part_list_size);
        long *vec;
        
        gsl_rng_set(r, c->seed + 1);
        
        for (unsigned long i = 0; i < c->part_list_size; ++i) {
                vec = malloc(sizeof(unsigned long) * 3);
                vec[0] = gen_num_beta(r, c->max_job_size, c->part_a, c->part_b);
                vec[1] = gen_num_beta(r, c->part_max_time, c->time_a, c->time_b);
                // printf("%lu [%ld %ld]\n", i, vec[0], vec[1]);
                result[i] = vec;
        }
        gsl_rng_free(r);
        return result;
}

void update_utilization(struct graph *g)
{
        double cur_node_utilization;
        cur_node_utilization = (g->time - g->prev_time)
                * g->alloc_nodes / (double) g->len;
        g->node_utilization += cur_node_utilization;
}

void jobs_dec_time_by(struct list *job_time_list, long time)
{
        struct node *iter;
        unsigned long *vec;

        for (iter = job_time_list->first; iter != NULL; iter = iter->next) {
                vec = iter->item;
                vec[1] -= time;
        }
}

long jobs_min_time(struct list *job_time_list)
{
        long min = BIG_NUM;
        long time;
        struct node *iter;

        for (iter = job_time_list->first; iter != NULL; iter = iter->next) {
                time = ((unsigned long*) iter->item)[1];
                if (time < min) {
                        min = time;
                }
        }
        return min;
}

static unsigned long time_delta(struct timeval prev_tv, struct timeval post_tv)
{
	time_t delta_sec = post_tv.tv_sec - prev_tv.tv_sec;
	suseconds_t delta_usec = post_tv.tv_usec - prev_tv.tv_usec;
	return 1000000 * delta_sec + delta_usec;
}

void jobs_remove_timeouts(FILE *f,
                          const struct config *cfg,
                          struct graph *g,
                          struct list *alloc_jobs)
{
        struct node *iter;
        long time, job_size, id;
        struct timeval prev_tv, post_tv;
        unsigned long dealloc_delta;

        for (iter = alloc_jobs->first; iter != NULL; iter = iter->next) {
                job_size = ((long *) iter->item)[0];
                time = ((long *) iter->item)[1];
                id = ((long *) iter->item)[2];

                if (time <= 0) {
                        g->alloc_nodes -= job_size;

                        gettimeofday(&prev_tv, NULL);
                        dealloc_job(g, job_size, id);
                        gettimeofday(&post_tv, NULL);

                        dealloc_delta = time_delta(prev_tv, post_tv);
                        update_metric(&dealloc_time, dealloc_delta);
                        total_dealloc += dealloc_delta;
                        
                        ++g->event;
                        if (cfg->verbose) {
                                fprintf(f, "D ");
                                fprintg(f, g);
                                fprintf(f, "p %ld %ld\n", job_size, id);
                        }
                        list_remove(alloc_jobs, iter);
                }
        }
}

static void fprintm(FILE *restrict s,
                    const char *msg,
                    const struct metric *m)
{
        /* fprintf(s, "%s metric:\n", msg); */
        fprintf(s, "%s min: %lu\n", msg,
                m->min == ULONG_MAX ? 0 : m->min);
        fprintf(s, "%s max: %lu\n", msg, m->max);
        fprintf(s, "%s avg: %lf\n", msg, m->avg);
}

int job_info(const struct config *cfg)
{
	long **part_list, *vec, size;
        long unsigned *max_dim_size;
        gsl_rng *r;
        unsigned long i, j;
	char fname[512];
	FILE *f;
	
	sprintf(fname, "%ldd_%ld_%ld_%ld.csv", cfg->n, cfg->part_a, cfg->part_b, cfg->seed);
	f = fopen(fname, "w+");

	long min_dim, min_dim_inner, min_dim_outer;
	long max_dimm, max_dim_inner, max_dim_outer;

        part_list = malloc(sizeof(long *) * cfg->part_list_size);

        r = gsl_rng_alloc(gsl_rng_taus);
        gsl_rng_set(r, cfg->seed + 1);
        srand(cfg->seed);

        max_dim_size = max_dim(cfg);
	fprintf(f, "index,job-size,min-dim,min-dim-inner,min-dim-outer,min-dim-outer-ratio,min-dim-internal-frag,"
	       "max-dim,max-dim-inner,max-dim-outer,max-dim-outer-ratio,max-dim-internal-frag");
	for (j = 0; j < cfg->n; ++j) {
		fprintf(f, ",k%ld", j);
	}
	fprintf(f, "\n");

        for (i = 0; i < cfg->part_list_size; ++i) {
                vec = malloc(sizeof(cfg->k) * (cfg->n+1));
                size = gen_num_beta(r, cfg->max_job_size, cfg->part_a, cfg->part_b);
		
                gen_min_dim_policy(vec, max_dim_size, cfg, size);
                vec[cfg->n] = gen_num_beta(r, cfg->part_max_time, cfg->time_a, cfg->time_b)* cfg->speed_factor;
                part_list[i] = vec;

		min_dim = mul(vec, cfg->n);
		min_dim_inner = 1;
		for (j = 0; j < cfg->n; ++j) {
			if (vec[j] <= 2) {
				min_dim_inner = 0;
				break;
			}
			min_dim_inner *= vec[j] - 2;
		}
		min_dim_outer = min_dim - min_dim_inner;
		
		gen_max_dim_policy(vec, max_dim_size, cfg, size);

		max_dimm = mul(vec, cfg->n);
		max_dim_inner = 1;
		for (j = 0; j < cfg->n; ++j) {
			if (vec[j] <= 2) {
				max_dim_inner = 0;
				break;
			}
			max_dim_inner *= vec[j] - 2;
		}
		max_dim_outer = max_dimm - max_dim_inner;
		
		/* "index,job-size,min-dim,min-dim-inner,min-dim-outer,min-dim-outer-ratio,min-dim-internal-frag,"
		   "max-dim,max-dim-inner,max-dim-outer,max-dim-outer-ratio,max-dim-internal-frag\n" */

		fprintf(f, "%lu,%ld,%ld,%ld,%ld,%lf,%lf,%ld,%ld,%ld,%lf,%lf", i, size,
		       min_dim, min_dim_inner, min_dim_outer,
		       (double) min_dim_outer / min_dim, (double) size / min_dim,
		       max_dimm, max_dim_inner, max_dim_outer,
		       (double) max_dim_outer / max_dimm, (double) size / max_dimm);
		for (j = 0; j < cfg->n; ++j) {
			fprintf(f, ",%ld", vec[j]);
		}
		fprintf(f, "\n");
        }

        gsl_rng_free(r);
        free(max_dim_size);
	for (i = 0; i < cfg->part_list_size; ++i) {
		free(part_list[i]);
	}
	free(part_list);
	fclose(f);
        return 0;
}

int non_contiguous(const struct config *cfg)
{
        long **job_list;
        struct graph g;
        
        struct list alloc_jobs;
        struct timeval prev_tv, post_tv;

        unsigned long i;

        unsigned long delta_time;
        unsigned long alloc_delta = 0;

        bool is_full;
        char *fname;
        FILE *f;

        fname = gen_filename(cfg);
        f = fopen(fname, "w+");
        fprintf(f, "%s\n", fname);
        job_list = gen_job_nodes(cfg);

        graph_alloc(&g, cfg->k, cfg->n);
        list_alloc(&alloc_jobs, cfg->part_list_size);

        for (i = 0; i < cfg->part_list_size; ++i) {
                g.prev_time = g.time;
                gettimeofday(&prev_tv, NULL);
                is_full = !try_fit(&g, job_list[i][0], i+1);
                gettimeofday(&post_tv, NULL);
                alloc_delta += time_delta(prev_tv, post_tv);

                if (!is_full) {
                        job_list[i][2] = i+1;
                        g.time += 1;
                        update_utilization(&g);

                        update_metric(&psize, job_list[i][0]);
                        g.alloc_nodes += job_list[i][0];
                        list_insert(&alloc_jobs, job_list[i]);

                        update_metric(&alloc_time, alloc_delta);
                        total_alloc += alloc_delta;
                        alloc_delta = 0;

                        jobs_dec_time_by(&alloc_jobs, 1);
                        ++g.event;
                        if (cfg->verbose) {
                                fprintf(f, "A ");
                                fprintg(f, &g);
                                fprintf(f, "p %ld %ld\n", job_list[i][0], i+1);
                        }
                        
                } else {
                        delta_time = jobs_min_time(&alloc_jobs);
                        update_metric(&pwait, delta_time);
                        jobs_dec_time_by(&alloc_jobs, delta_time);
                        i--;
                        g.time += delta_time;
                        update_utilization(&g);
                }
                jobs_remove_timeouts(f, cfg, &g, &alloc_jobs);
        }
        while (alloc_jobs.size != 0) {
                delta_time = jobs_min_time(&alloc_jobs);
                jobs_dec_time_by(&alloc_jobs, delta_time);

                g.prev_time = g.time;
                g.time += delta_time;

                update_utilization(&g);
                jobs_remove_timeouts(f, cfg, &g, &alloc_jobs);
        }
        fprintf(f, "node utilization: %lf\n", g.node_utilization / g.time);
        fprintf(f, "edge utilization: %lf\n", g.edge_utilization / g.time);
        fprintf(f, "part utilization: %lf\n", g.edge_loss / (2 * cfg->part_list_size));

        psize.avg /= cfg->part_list_size;
        fprintm(f, "part size", &psize);
        
        ifrag.avg /= cfg->part_list_size;
        fprintm(f, "real part size", &ifrag);

        ptime.avg /= cfg->part_list_size;
        fprintm(f, "part time", &ptime);
        
        pwait.avg /= cfg->part_list_size;
        fprintm(f, "part wait", &pwait);

        afails.avg /= cfg->part_list_size;
        fprintm(f, "alloc fails", &afails);

        afails_rot.avg /= cfg->part_list_size;
        fprintm(f, "alloc fails rot", &afails_rot);

        fprintf(f, "simulation time: %lu\n", g.time);

        long unsigned total_time = total_alloc + total_dealloc;

	fprintf(f, "alloc time: %lu\n", total_alloc);
        alloc_time.avg = (float) total_alloc / cfg->part_list_size;
	fprintm(f, "alloc time", &alloc_time);

	fprintf(f, "dealloc time: %lu\n", total_dealloc);
	dealloc_time.avg = (float) total_dealloc / cfg->part_list_size;
	fprintm(f, "dealloc time", &dealloc_time);

        fprintf(f, "total time: %lu\n", total_time);

	printf("alloc time avg: %.0lfus\n", alloc_time.avg);
	printf("dealloc time avg: %.0lfus\n", dealloc_time.avg);

        for (i = 0; i < cfg->part_list_size; ++i) {
                free(job_list[i]);
        }
        fclose(f);
        free(fname);
        graph_free(&g);
        free(job_list);
        list_free(&alloc_jobs);

        return 0;
}


