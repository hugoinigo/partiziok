#ifndef GRAPH_H_
#define GRAPH_H_

#include <stdio.h>

struct graph {
        size_t len;
        size_t n;
        unsigned long e;

        unsigned long time;
        unsigned long prev_time;
        unsigned long event;

        unsigned long lost_edges;
        unsigned long alloc_nodes;
        unsigned long alloc_edges;

        double node_utilization;
        double edge_utilization;
        double edge_loss;

        unsigned long *data;
        const long *k;
};

/* Get the graph element at the given coords and
 * an optional offset, if offset is NULL it will be
 * ommited.
 */
unsigned long *graph_elem(const struct graph *g,
                          const long *offset,
                          const long *coords);

/* Set a graph's region to the given state. The region
 * is given by the initial coords and the partition.
 */
void set_graph(struct graph *g,
               const long *coords,
               const long *part,
               unsigned long id);

/* Initialize the given graph with the given parameters:
 * n dimensions and k length for each dimension.
 */
void graph_alloc(struct graph *g,
                 const long *k,
                 size_t n);

/* Free all the resources from the graph.
 */
void graph_free(struct graph *g);

/* Print a graph to the given file.
 */
void fprintg(FILE *restrict s, const struct graph *g);

#endif /* GRAPH_H_ */
