#include "util.h"

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

void parse_arg(const char *arg, long *result)
{
        char *error;
        assert(*arg != '\0' && "empty string");

        *result = strtol(arg, &error, 10);
        assert(*error == '\0' && "error occurred parsing");
}

void parse_arg_f(const char *arg, float *result)
{
        char *error;
        assert(*arg != '\0' && "empty string");

        *result = strtod(arg, &error);
        assert(*error == '\0' && "error occurred parsing");
}

void fill_dim_mask(long unsigned num,
                   bool *bitmask,
                   const long unsigned n)
{
        long unsigned i;
        unsigned bit;
        for (i = 0; i < n; ++i) {
                bit = num & 1;
                if (bit) {
                        bitmask[i] = true;
                } else {
                        bitmask[i] = false;
                }
                num >>= 1;
        }
}

void parse_params(const char *const *argv, struct config *c)
{
        long result;
        unsigned res_bits = sizeof(result) * 8;
        unsigned argc = 0;

        parse_arg(argv[argc++], &result);
        assert(result >= 1 && "n is less than 1");
        c->n = result;

        c->k = malloc(sizeof(unsigned long) * (c->n));
        c->dim_mask = malloc(sizeof(bool) * (c->n));

        parse_arg(argv[argc++], &result);
        assert(result >= 1 && "count is less than 1");
        c->part_list_size = result;

        parse_arg(argv[argc++], &result);
        assert(result > 0 && "part alpha is less than 0");
        c->part_a = result;

        parse_arg(argv[argc++], &result);
        assert(result > 0 && "part beta is less than 0");
        c->part_b = result;

        parse_arg(argv[argc++], &result);
        assert(result > 0 && "range is less than 1");
        c->part_max_time = result;

        parse_arg(argv[argc++], &result);
        assert(result > 0 && "time alpha is less than 0");
        c->time_a = result;

        parse_arg(argv[argc++], &result);
        assert(result >= 1 && "time beta is less than 1");
        c->time_b = result;

        parse_arg(argv[argc++], &result);
        /* we only care for the least significant n bits */
        result = (unsigned long) (result << (res_bits - c->n)) >> (res_bits - c->n);
        assert(result >= 1 && "dim mask is less than 1");
        fill_dim_mask(result, c->dim_mask, c->n);
        /* print_bitmask(c->dim_mask, c->n); */

        parse_arg(argv[argc++], &result);
        assert(result >= 0 && "seed is less than 0");
        c->seed = result;

        if (argv[argc++][0] == 't') {
                c->verbose = 1;
        } else {
                c->verbose = 0;
        }

        c->network_size = 1;
        for (unsigned i = 0; i < c->n; ++i) {
                parse_arg(argv[i + argc], &result);
                assert(result >= 2 && "k is less than 2");
                (c->k)[i] = result;
                c->network_size *= result;
        }
}

char *gen_filename(const struct config *c)
{
        unsigned offs = 0;
        char *fn = malloc(sizeof(char) * 1000);

        offs += sprintf(fn, "%lu_%lu_%lu_%lu_%lu_%lu_%lu",
                        c->n, c->part_list_size, c->part_a, c->part_b,
                        c->part_max_time, c->time_a, c->time_b);
        offs += sprintf(fn + offs, "_%.3f", c->speed_factor);
        offs += sprintf(fn + offs, "_%lu", c->seed);
        offs += sprintf(fn + offs, "_%lu", c->max_job_size);
        offs += sprintf(fn + offs, "_%lu", c->rotate);
        offs += sprintf(fn + offs, "_%s", c->part_policy);

        const char *mode;
        if (strcmp(c->sched_policy, "fr") == 0) {
                if (c->fr_criterion == FR_NONE) {
                        mode = "fr";
                } else {
                        mode = c->fr_crit_desc;
                }
        } else {
                mode = c->sched_policy;
        }
        offs += sprintf(fn + offs, "_%s", mode);

        for (unsigned i = 0; i < c->n; ++i) {
                offs += sprintf(fn + offs, "_%lu", c->k[i]);
        }

        offs += sprintf(fn + offs, ".txt");
        fn[offs+1] = '\0';

        return fn;
}

/* Iterate the given coords over the given partition.
 */
void iter_coords(long *coords,
                 const long *max_ind,
                 size_t n)
{
        unsigned long i;
        for (i = 0; i < n; ++i) {
                if (coords[i] + 1 < max_ind[i]) {
                        coords[i] += 1;
                        break;
                }
                /* coord i + 1 == max_ind */
                coords[i] = 0;
        }
}

unsigned long part_count_edges(const long *k,
                               size_t n)
{
        unsigned long i, j;
        unsigned long aux, res = 0;

        for (i = 0; i < n; ++i) {
                aux = 1;
                for (j = 0; j < i; ++j) {
                        aux *= k[j];
                }
                aux *= k[i] - 1;
                for (j = i+1; j < n; ++j) {
                        aux *= k[j];
                }
                res += aux;
        }

        return res;
}

inline unsigned int root(unsigned int input, unsigned int n)
{
    
        return((unsigned int)floor(pow(input, 1. / n)));
}

long mul(long *vec, unsigned long n)
{
    unsigned int i;
    long aux = 1;

    for (i = 0; i < n; i++) {
        aux *= vec[i];
    }

    return(aux);
}
