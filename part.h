#ifndef PART_H_
#define PART_H_

#include <stdio.h>

struct part {
        long *coords;
        long *dimensions;
        long time;
        struct part *next;
        unsigned long links;
};

void fprintp(FILE *restrict s,
             const struct part *p,
             const unsigned long n);

/* Free all the resources from the partition.
 */
void part_free(struct part *part);

void part_append(struct part **head, struct part *p);

void part_free_all(struct part **head);

#endif /* PART_H_ */
