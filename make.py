#!/usr/bin/env python3
from pybuild import *

CC = 'gcc'
CFLAGS = '-Wall -Wpedantic'
LFLAGS = '-lgsl -lgslcblas -lm'

TARGET = 'partiziok'

SRC_DIR = './'
BUILD_DIR = './build/'

MODULES = 'edges gen graph part util permutation args metric mc ff main fr region list array nc log'.split(' ')

def mkdir_p(path):
    dir_target = Target(path)
    dir_target.cmd_append(Command(f'mkdir -p {path}'))
    dir_target.exec()

def modules_build():
    for modname in MODULES:
        target_name = strcat(BUILD_DIR, modname, '.o')
        source_name = strcat(SRC_DIR, modname, '.c')

        mod_target = Target(target_name, deps=[source_name])
        mod_target.cmd_append(Command(f'{CC} {CFLAGS} -c {source_name} -o {target_name}'))
        mod_target.exec()

def partiziok_build():
    obj_files = pref_suff_set(MODULES, pref=BUILD_DIR, suff='.o')

    partiziok_target = Target(TARGET, deps=obj_files)
    partiziok_target.cmd_append(Command(f'{CC} {CFLAGS} -o {TARGET} {strcat(*obj_files, sep=" ")} {LFLAGS}'))
    partiziok_target.exec()

if __name__ == '__main__':
    mkdir_p(BUILD_DIR)
    modules_build()
    partiziok_build()
    
