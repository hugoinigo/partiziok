#ifndef __ARRAY_H
#define __ARRAY_H

#include <stdio.h>
#include <stdbool.h>

long array_sum(const long *vec, unsigned len);

long array_prod(const long *vec, unsigned len);

void array_set(long *vec,
	       unsigned len,
	       long num);

bool array_all(const long *vec,
	       unsigned len,
	       long eq);

bool array_any(const long *vec,
	       unsigned len,
	       long eq);

void array_print(const long *vec, unsigned len);

void array_fprint(FILE *stream,
		  const long *vec,
		  unsigned len);

#endif // VECTOR_H_
