#ifndef __REGION_H
#define __REGION_H

#include <stdbool.h>
#include <malloc.h>

struct reg {
	size_t n;
	int time;
	int id;
	long *coords;
	long *dims;
};

void reg_copy(struct reg *dest, const struct reg *src);
struct reg *reg_alloc(size_t n);
struct reg *reg_clone(const struct reg *reg);
void reg_free(struct reg *r);
void reg_fill(struct reg *r, size_t n);
void reg_empty(struct reg *r);
void reg_set(struct reg *dest,
	     const long *coords,
	     const long *dims,
	     size_t n,
	     int time);

/* true if `a` is subregion of `b`
 * false otherwise
 */
bool reg_subreg(const struct reg *a, const struct reg *b);

/* true if m1 and m2 overlap
 * false otherwise
 */
bool reg_overlap(const struct reg *a, const struct reg *b);

bool reg_adjacent(const struct reg *a, const struct reg *b, int *dim);

bool reg_semi_adjacent(const struct reg *a, const struct reg *b, int *dim);
/* get the intersection submesh of `a` and `b`
 * into `dest`.
 */
void reg_intersection(const struct reg *a,
		      const struct reg *b,
		      struct reg *dest);

void reg_union(const struct reg *a,
               const struct reg *b,
               struct reg *dest);

void reg_intersection_dim(const struct reg *a,
			  const struct reg *b,
			  struct reg *dest,
			  size_t dim);

void reg_union_dim(const struct reg *a,
		   const struct reg *b,
		   struct reg *dest,
		   size_t dim);

void reg_print(const struct reg *reg);

/* Split the given `reg` mesh region according
 * to the allocation of `job`. All the subregions
 * are stored in `subregs`. The return value is the
 * number of new subregions.
 */
size_t quadrants(const struct reg *reg,
		 const struct reg *job,
		 struct reg ***subregs);

#endif // __REGION_H
