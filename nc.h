#ifndef NC_H_
#define NC_H_
#include "util.h"

int non_contiguous(const struct config *cfg);
int job_info(const struct config *cfg);

#endif /* NC_H_ */
