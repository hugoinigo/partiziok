#include "part.h"
#include <malloc.h>

/* Free all the resources from the partition.
 */
void part_free(struct part *part)
{
        free(part->coords);
        free(part->dimensions);
        free(part);
}

void fprintp(FILE *restrict s,
             const struct part *p,
             const unsigned long n)
{
        unsigned long i;
        fprintf(s, "p");
        for (i = 0; i < n; ++i) {
                fprintf(s, " %lu", p->dimensions[i]);
        }
        fprintf(s, "\n");
}

void print_part(const struct part *p, const unsigned long n)
{
        unsigned long i;
        printf("--\ncoords ->");
        for (i = 0; i < n; ++i) {
                printf(" %lu", p->coords[i]);
        }
        printf("\npart ->");
        for (i = 0; i < n; ++i) {
                printf(" %lu", p->dimensions[i]);
        }
        printf("\ntime -> %lu\n", p->time);
}

void part_append(struct part **head, struct part *p)
{
        struct part **i = head;
        while (*i != NULL) {
                i = &(*i)->next;
        }
        *i = p;
}

void part_free_all(struct part **head)
{
        struct part **i = head;
        struct part *next;
        while (*i != NULL) {
                next = (*i)->next;
                part_free(*i);
                *i = next;
        }
}
