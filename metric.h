#ifndef __METRIC_H
#define __METRIC_H

struct metric {
        unsigned long min;
        unsigned long max;
        double avg;
};

struct metricd {
	double min;
	double max;
	double avg;
};

extern struct metric psize;

extern struct metric ptime;

extern struct metric pwait;

extern struct metric ifrag;

extern struct metric afails;

extern struct metric afails_rot;

extern struct metric reg_list_size;

extern struct metricd alloc_time;

extern struct metricd dealloc_time;

//extern struct metric total_time;

void update_metric(struct metric *m, unsigned long size);
void update_metricd(struct metricd *m, double size);
#endif
