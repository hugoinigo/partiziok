#include "region.h"
#include "array.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

static inline long min(long a, long b)
{
	if (a < b) {
		return a;
	} else {
		return b;
	}
}

static inline long max(long a, long b)
{
	if (a > b) {
		return a;
	} else {
		return b;
	}
}

void reg_fill(struct reg *r, size_t n)
{
        r->dims = malloc(sizeof(long) * n);
        r->coords = calloc(sizeof(long), n);
        r->n = n;
	r->time = 0;
}

void reg_empty(struct reg *r)
{
        free(r->dims);
        free(r->coords);
}

struct reg *reg_alloc(size_t n)
{
        struct reg *r = malloc(sizeof(struct reg));
        // TODO: Check NULL
        reg_fill(r, n);
        return r;
}

void reg_set(struct reg *dest,
	     const long *coords,
	     const long *dims,
	     size_t n,
	     int time)
{
	memcpy(dest->coords, coords, sizeof(long) * n);
	memcpy(dest->dims, dims, sizeof(long) * n);
	dest->n = n;
	dest->time = time;
}

void reg_print(const struct reg *reg)
{
	printf("n: %ld\n", reg->n);
	/* printf("c"); */
	/* array_print(reg->coords, reg->n); */
	/* printf(" d"); */
	/* array_print(reg->dims, reg->n); */
	/* printf(" %d", reg->time); */
}

void reg_free(struct reg *r)
{
        reg_empty(r);
        free(r);
}

void reg_copy(struct reg *dest, const struct reg *src)
{
	reg_set(dest, src->coords, src->dims, src->n, src->time);
	/*
	size_t i;
	dest->n = src->n;
	dest->time = src->time;
	memcpy(dest->coords, src->coords, sizeof(long) * src->n);
        memcpy(dest->dims, src->dims, sizeof(long) * src->n);	

	for (i = 0; i < src->n; i++) {
		dest->coords[i] = src->coords[i];
		dest->dims[i] = src->dims[i];
	}
	*/
}

struct reg *reg_clone(const struct reg *reg)
{
	struct reg *result = malloc(sizeof(struct reg));
	result->coords = malloc(sizeof(long) * reg->n);
	result->dims = malloc(sizeof(long) * reg->n);

        reg_copy(result, reg);
	return result;
}


/* true if `a` is subregion of `b`
 * false otherwise
 */
bool reg_subreg(const struct reg *a, const struct reg *b)
{
	size_t i;
	bool v, u;
	assert(a->n == b->n && "Regions of different dimensions.");
  
	for (i = 0; i < a->n; ++i) {
		v = a->coords[i] >= b->coords[i];
		u = a->coords[i] - b->coords[i] <= b->dims[i] - a->dims[i];
		if (!v || !u) {
			return false;
		}
	}
	return true;
}

bool reg_subreg_skip(const struct reg *a,
                    const struct reg *b,
                    size_t skip)
{
	size_t i;
	bool v, u;
	assert(a->n == b->n && "Regions of different dimensions.");

	for (i = 0; i < a->n; ++i) {
                if (i == skip) {
                        continue;
                }
		v = a->coords[i] >= b->coords[i];
		u = a->coords[i] - b->coords[i] <= b->dims[i] - a->dims[i];
		if (!v || !u) {
			return false;
		}
	}
	return true;
}


/* true if m1 and m2 overlap
 * false otherwise
 */
bool reg_overlap(const struct reg *a, const struct reg *b)
{
	size_t i;
	bool v, u;
	assert(a->n == b->n && "Regions of different dimensions.");
  
	for (i = 0; i < a->n; ++i) {
		v = a->coords[i] - b->coords[i] < b->dims[i];
		u = b->coords[i] - a->coords[i] < a->dims[i];
		if (!v || !u) {
			return false;
		}
	}
	return true;
}

bool reg_overlap_skip(const struct reg *a,
		     const struct reg *b,
		     size_t skip)
{
	size_t i;
	bool v, u;
	assert(a->n == b->n && "Regions of different dimensions.");

	for (i = 0; i < a->n; ++i) {
		if (i == skip) {
			continue;
		}
		v = a->coords[i] - b->coords[i] < b->dims[i];
		u = b->coords[i] - a->coords[i] < a->dims[i];
		if (!v || !u) {
			return false;
		}
	}
	return true;
}

/* in worst case this is O(2*n^2)
 * WE HAVE TO THINK ABOUT THIS
 * try to find out NOT ADJACENT,
 * i.e. the minimum condition to
 * DISCARD adjacency
 */
bool reg_adjacent(const struct reg *a, const struct reg *b, int *dim)
{
/* we need a quick method to DISCARD adjacent regions
 */
        size_t i;
        bool a_sub_b, aux_b, aux_a;
        assert(a->n == b->n && "Regions of different dimensions.");
        for (i = 0; i < a->n; ++i) {
                a_sub_b = reg_subreg_skip(a, b, i);
                /* if (disc && a->coords[i] + a->dims[i] == b->coords[i]) { */
		/* 	if (dim != NULL) { */
		/* 		*dim = 2*i; */
		/* 	} */
                /*         return true; */
                /* } */
                //b_sub_a = is_subreg_skip(b, a, i);
		aux_a = a->coords[i] + a->dims[i] == b->coords[i];
		aux_b = b->coords[i] + b->dims[i] == a->coords[i];
		if (a_sub_b && (aux_a || aux_b)) {
			if (dim != NULL) {
				*dim = 2*i + (aux_b ? 1 : 0);
			}
                        return true;
		}
                /* if (disc && b->coords[i] + b->dims[i] == a->coords[i]) { */
		/* 	if (dim != NULL) { */
		/* 		*dim = 2*i + 1; */
		/* 	} */
                /*         return true; */
                /* } */
        }
        return false;
}

/*bool adjacent(const struct reg *a, const struct reg *b) {
	return adjacent2(a, b, NULL);
	}*/

bool reg_semi_adjacent(const struct reg *a, const struct reg *b, int *dim)
{
	size_t i;
	bool aux;
	/* printf("b"); */
	/* reg_print(b); */
	/* printf("\na"); */
	/* reg_print(a); */
	/* printf("\n"); */
	assert(a->n == b->n && "Regions of different dimensions.");
	for (i = 0; i < a->n; ++i) {
		aux = reg_overlap_skip(a, b, i);
		if (aux && a->coords[i] + a->dims[i] == b->coords[i]) {
			if (dim != NULL) {
				*dim = 2*i;
			}
			return true;
		}
		aux = reg_overlap_skip(b, a, i);
		if (aux && b->coords[i] + b->dims[i] == a->coords[i]) {
			if (dim != NULL) {
				*dim = 2*i + 1;
			}
			return true;
		}
	}
	return false;
}

/* get the intersection submesh of `a` and `b`
 * into `dest`.
 */
void reg_intersection(const struct reg *a,
		      const struct reg *b,
		      struct reg *dest)
{
	size_t i;
        long coord, dim;
	assert(a->n == b->n && "Regions of different dimensions.");
	for (i = 0; i < a->n; ++i) {
                coord = max(a->coords[i], b->coords[i]);
                dim = min(a->coords[i]+a->dims[i], b->coords[i]+b->dims[i]) - coord;
		dest->coords[i] = coord;
		dest->dims[i] = dim > 0 ? dim : 0;
	}
	dest->n = b->n;
}

void reg_union(const struct reg *a,
               const struct reg *b,
               struct reg *dest)
{
        size_t i;
        assert(a->n == b->n && "Regions of different dimensions.");
        for (i = 0; i < a->n; ++i) {
                dest->coords[i] = min(a->coords[i], b->coords[i]);
                dest->dims[i] = max(a->coords[i] + a->dims[i], b->coords[i] + b->dims[i]) - dest->coords[i];
        }
        dest->n = b->n;
}

void reg_intersection_dim(const struct reg *a,
			  const struct reg *b,
			  struct reg *dest,
			  size_t dim)
{
	long coord_aux, dim_aux;
	size_t i;
	assert(a->n == b->n && "Regions of different dimensions.");

	for (i = 0; i < a->n; ++i) {
		if (i == dim) {
			continue;
		}
                coord_aux = max(a->coords[i], b->coords[i]);
                dim_aux = min(a->coords[i]+a->dims[i], b->coords[i]+b->dims[i]) - coord_aux;
		dest->coords[i] = coord_aux;
		dest->dims[i] = dim_aux > 0 ? dim_aux : 0;
	}
	dest->n = b->n;
}

void reg_union_dim(const struct reg *a,
		   const struct reg *b,
		   struct reg *dest,
		   size_t dim)
{
	long coord_aux, dim_aux;
        assert(a->n == b->n && "Regions of different dimensions.");

	coord_aux = min(a->coords[dim], b->coords[dim]);
	dim_aux = max(a->coords[dim] + a->dims[dim], b->coords[dim] + b->dims[dim]) - coord_aux;

	dest->coords[dim] = coord_aux;
	dest->dims[dim] = dim_aux;
}

/* Split the given `reg` mesh region according
 * to the allocation of `job`. All the subregions
 * are stored in `subregs`. The return value is the
 * number of new subregions.
 */
size_t quadrants(const struct reg *reg,
		 const struct reg *job,
		 struct reg ***subregs)
{
	long *a_coords;
	long *a_dims;
	long *b_coords;
	long *b_dims;
	size_t i, j;
	size_t count = 0;
	const size_t n = reg->n;

	struct reg **reg_iter = malloc(sizeof(struct reg *) * 2*n);
	struct reg *new_reg;
	
	assert(reg->n == job->n && "Regions of different dimensions.");
	for (j = 0; j < n; ++j) {
		a_coords = malloc(sizeof(long) * n);
		a_dims = malloc(sizeof(long) * n);
		b_coords = malloc(sizeof(long) * n);
		b_dims = malloc(sizeof(long) * n);
		for (i = 0; i < n; ++i) {
			a_coords[i] = reg->coords[i];
			a_dims[i] = reg->dims[i];
			b_coords[i] = reg->coords[i];
			b_dims[i] = reg->dims[i];
		}
		// a_coords[j] = reg->coords[j];
		a_dims[j] = job->coords[j] - reg->coords[j];
		b_coords[j] = job->coords[j] + job->dims[j];
		b_dims[j] = (reg->coords[j] + reg->dims[j]) - (job->coords[j] + job->dims[j]);
		if (!array_any(a_dims, n, 0)) {
			// gorde
			new_reg = malloc(sizeof(struct reg));
			// TODO: check NULL
			new_reg->n = n;
			new_reg->time = 0;
			new_reg->coords = a_coords;
			new_reg->dims = a_dims;
			reg_iter[count++] = new_reg;
		} else {
			free(a_coords);
			free(a_dims);
		}
		if (!array_any(b_dims, n, 0)) {
			// gorde
			new_reg = malloc(sizeof(struct reg));
			// TODO: check NULL
			new_reg->n = n;
			new_reg->time = 0;
			new_reg->coords = b_coords;
			new_reg->dims = b_dims;
			reg_iter[count++] = new_reg;
		} else {
			free(b_coords);
			free(b_dims);
		}
	}
	*subregs = reg_iter;
	return count;
}
