#ifndef EDGES_H_
#define EDGES_H_

#include "graph.h"
#include "part.h"

void edge_mem_alloc(size_t n);
void edge_mem_free();

unsigned long count_lost_edges(const struct part *p, const struct graph *g);
long count_lost_edges_a(const struct graph *g,
			const long *coords,
			const long *dims,
			size_t n);

#endif /* EDGES_H_ */
