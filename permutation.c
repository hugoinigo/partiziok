#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define N 4

/*
void fill_dest(long *dest,
               const long *orig,
               size_t n)
{
        long unsigned i;
        for (i = 0; i < n; ++i) {
                dest[i] = orig[i];
        }
}
*/

long get_ind(long unsigned ind,
             long *vec,
             size_t n)
{
        long res = vec[ind];
        size_t i;
        
        for (i = ind; i < n-1; ++i) {
                vec[i] = vec[i+1];
        }
        
        return res;
}

void print_permu(long *perm, const long unsigned n)
{
        long unsigned i;
        for (i = 0; i < n; ++i) {
                printf("%ld ",perm[i]);
        }
        printf("\n");
}

unsigned long factorial(const long unsigned n)
{
        return !n ? 1 : n * factorial(n-1);
}

void lexicographical_permutation(long *dest,
                                 long unsigned perm_num,
                                 const long *part,
                                 size_t n)
{
        /* VLA -> hobeto erabili malloc? */
        long unsigned factorand[n];
        long aux[n];
        long unsigned index;
        long i;

        memcpy(aux, part, sizeof(*aux) * n);

        for (i = n - 1; i >= 0; i--) {
                factorand[i] = perm_num % (n - i);
                perm_num /= (n - i);
        }
        for (i = 0; i < n; i++) {
                index = factorand[i];
                dest[i] = get_ind(index, aux, n - i);
        }
}

/*
int main(void)
{
        const long part[N] = {1, 2, 3, 4};//, 5, 6};
        long res[N];
        unsigned index;
        unsigned i, perm_num;
        int j;

        / *
        factorials[0] = 1;

        for (i = 1; i <= N; ++i) {
                factorials[i] = factorials[i-1] * i;
        }

        for (i = 0; i < factorials[N]; ++i) {
                fill_dest(aux, part, N);
                perm_num = i;
                for (j = N-1; j >= 0; --j) {
                        factorand[j] = perm_num % (N-j);
                        perm_num /= (N-j);
                }
                for (j = 0; j < N; ++j) {
                        index = factorand[j];
                        res[j] = get_ind(index, aux, N-j);
                }
                print_perm(res, N);
        }
        * /
        long unsigned fact = factorial(N);
        for (i = 0; i < fact; ++i) {
                lexicographical_permutation(res, i, part, N);
                print_permu(res, N);
        }

        return 0;
}
*/
