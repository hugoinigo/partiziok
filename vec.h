#ifndef __HG_VEC_H
#define __HG_VEC_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdlib.h>
#include <assert.h>

struct hg_vec {
	size_t length;
	size_t capacity;
	void **data;
};

int hg_vec_alloc(struct hg_vec *vec, size_t capacity);
void hg_vec_free(struct hg_vec *vec);
int hg_vec_push(struct hg_vec *vec, const void *item);
void *hg_vec_pop(struct hg_vec *vec);
int hg_vec_insert(struct hg_vec *vec,
		  const void *item,
		  size_t index);
void *hg_vec_remove(struct hg_vec *vec, size_t index);
void *hg_vec_get(struct hg_vec *vec, size_t index);
void hg_vec_set(struct hg_vec *vec,
		size_t index,
		const void *item);
void hg_vec_sort(struct hg_vec *vec,
		 int (*compar) (const void *, const void *));
void hg_vec_sort_r(struct hg_vec *vec,
		   int (*compar)(const void *, const void *, void *),
		   void *arg);

#endif /* __HG_VEC_H */

#ifdef HG_VEC_IMPLEMENTATION

/* void data_realloc(void **data, size_t size) */
/* { */
/* } */

#define HG_VEC_DEF_CAP 128

int hg_vec_alloc(struct hg_vec *vec, size_t capacity)
{
	void **data;

	assert(vec != NULL && "The given vector is NULL");
	if (capacity == 0) {
		capacity = HG_VEC_DEF_CAP;
	}
	data = malloc(capacity * sizeof(void *));

	if (data == NULL) {
		return -1;
	}

	vec->capacity = capacity;
	vec->length = 0;
	vec->data = data;

	return 0;
}

void hg_vec_free(struct hg_vec *vec)
{
	free(vec->data);
}

int hg_vec_push(struct hg_vec *vec, const void *item)
{
	size_t new_cap;
	assert(vec != NULL && "The given vector is NULL");

	if (vec->length >= vec->capacity) {
		new_cap = vec->capacity * 2;
		if (new_cap == 0) {
			new_cap = HG_VEC_DEF_CAP;
		}
		vec->data = realloc(vec->data, new_cap * sizeof(void *));
		if (errno == ENOMEM) {
			perror("hg_vec_push");
			exit(1);
		}
		vec->capacity = new_cap;
	}

	vec->data[vec->length] = (void *) item;
	vec->length += 1;

	return 0;
}

void *hg_vec_pop(struct hg_vec *vec)
{
	void *item;

	assert(vec != NULL && "The given vector is NULL");
	if (vec->length == 0) {
		return NULL;
	}
	vec->length -= 1;
	item = vec->data[vec->length];

	return item;
}

int hg_vec_insert(struct hg_vec *vec,
		   const void *item,
		   size_t index)
{
	size_t i, new_cap;

	assert(vec != NULL && "The given vector is NULL");
	if (vec->length >= vec->capacity) {
		new_cap = vec->capacity * 2;
		if (new_cap == 0) {
			new_cap = HG_VEC_DEF_CAP;
		}
		vec->data = realloc(vec->data, new_cap * sizeof(void *));
		if (errno == ENOMEM) {
			perror("hg_vec_insert");
			exit(1);
		}
		vec->capacity = new_cap;
	}
	if (index > vec->length - 1) {
		return -2;
	}
	for (i = vec->length; i > index; i--) {
		vec->data[i] = vec->data[i - 1];
	}
	vec->data[index] = (void *) item;
	vec->length += 1;

	return 0;
}

void *hg_vec_remove(struct hg_vec *vec, size_t index)
{
	void *item;
	size_t i;

	assert(vec != NULL && "The given vector is NULL");
	if (vec->length == 0) {
		return NULL;
	}
	if (index > vec->length - 1) {
		return NULL;
	}

	item = vec->data[index];
	vec->length -= 1;
	for (i = index; i < vec->length; i++) {
		vec->data[i] = vec->data[i+1];
	}
	return item;
}

void *hg_vec_get(struct hg_vec *vec, size_t index)
{
	assert(vec != NULL && "The given vector is NULL");
	if (index > vec->length - 1) {
		return NULL;
	}
	return vec->data[index];
}

void hg_vec_set(struct hg_vec *vec,
		 size_t index,
		 const void *item)
{
	assert(vec != NULL && "The given vector is NULL");
	if (index > vec->length - 1) {
		return;
	}
	vec->data[index] = (void *) item;
}

struct compar_aux {
	int (*compar) (const void *, const void *);
};

struct compar_aux_r {
	int (*compar) (const void *, const void *, void *);
	void *arg;
};

int cons_compar(const void *a,
		const void *b,
		void *cmp)
{
	return ((struct compar_aux *) cmp)->compar(* (const void **)a, *(const void **) b);
}

static inline int
cons_compar_r(const void *a,
	      const void *b,
	      void *cmp)
{
	return ((struct compar_aux_r *) cmp)->compar(*(const void **)a,
						     *(const void **)b,
						     ((struct compar_aux_r *) cmp)->arg);
}

void hg_vec_sort(struct hg_vec *vec,
		 int (*compar) (const void *, const void *))
{
	struct compar_aux cmp;

	cmp.compar = compar;
	qsort_r(vec->data, vec->length, sizeof(void *), cons_compar, &cmp);
}

void hg_vec_sort_r(struct hg_vec *vec,
		   int (*compar)(const void *, const void *, void *),
		   void *arg)
{
	struct compar_aux_r cmp;

	cmp.compar = compar;
	cmp.arg = arg;
	qsort_r(vec->data, vec->length, sizeof(void *), cons_compar_r, &cmp);
}

#endif /* HG_VEC_IMPLEMENTATION */

