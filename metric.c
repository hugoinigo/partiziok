#include "metric.h"
#include <limits.h>
#include <float.h>

struct metric psize = { .min = ULONG_MAX, .max = 0, .avg = 0. };
struct metric ptime = { .min = ULONG_MAX, .max = 0, .avg = 0. };
struct metric pwait = { .min = ULONG_MAX, .max = 0, .avg = 0. };
struct metric ifrag = { .min = ULONG_MAX, .max = 0, .avg = 0. };
struct metric afails = { .min = ULONG_MAX, .max = 0, .avg = 0. };
struct metric afails_rot = { .min = ULONG_MAX, .max = 0, .avg = 0. };

struct metricd alloc_time = { .min = DBL_MAX, .max = 0, .avg = 0. };
struct metricd dealloc_time = { .min = DBL_MAX, .max = 0, .avg = 0. };
//struct metric total_time = { .min = ULONG_MAX, .max = 0, .avg = 0. };
struct metric reg_list_size = { .min = ULONG_MAX, .max = 0, .avg = 0. };

void update_metric(struct metric *m, unsigned long size)
{
        m->avg += (double) size;
        if (size < m->min) {
                m->min = size;
        }
        if (size > m->max) {
                m->max = size;
        }
}

void update_metricd(struct metricd *m, double size)
{
        m->avg += size;
        if (size < m->min) {
                m->min = size;
        }
        if (size > m->max) {
                m->max = size;
        }
}

