#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include "list.h"

void log_array(FILE *f,
               const long *arr,
               size_t len);

void log_state(FILE *f,
               int time,
               const struct list *joblist,
               const struct list *reglist);

#endif /* _LOG_H */
