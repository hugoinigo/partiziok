#ifndef MC_H_
#define MC_H_
#include "util.h"

int monte_carlo(const struct config *cfg);

#endif /* MC_H_ */
