#!/bin/env bash

job_sizes=(2 5 5 5 5 2)

for i in $(seq 1000); do
    for job_size in 0 2 4; do
	job_a=${job_sizes[${job_size}]}
	job_b=${job_sizes[${job_size}+1]}
	eval "./ff -n 6 4 4 4 4 4 4 -p 1000 --size-alpha ${job_a} --size-beta ${job_b} -t 1000 --time-alpha 2 --time-beta 5 -s $i -gmin-dim -m1024 --speed-factor 1 --rotate 0 --mode ji"
    done;
done;

