#include "log.h"
#include "region.h"

void log_array(FILE *f,
               const long *arr,
               size_t len)
{
        int i;
        for (i = 0; i < len-1; i++) {
                fprintf(f, "%ld,", arr[i]);
        }
        if (len > 0) {
                fprintf(f, "%ld", arr[len-1]);
        }
}

void log_reg(FILE *f, const struct reg *reg)
{
        fputc('{', f);
	fprintf(f, "%d;", reg->id);
        log_array(f, reg->coords, reg->n);
        fputc(';', f);
        log_array(f, reg->dims, reg->n);
        fputc('}', f);
}

void log_reglist(FILE *f, const struct list *reglist)
{
        const struct node *iter;
        const struct reg *reg;
        int i = 0;
        for (iter = reglist->first; iter != NULL; iter = iter->next) {
                reg = iter->item;
                if (i == reglist->size - 1) {
                        break;
                }
                log_reg(f, reg);
                fputc(',', f);

                i += 1;
        }
        if (reglist->size > 0) {
                log_reg(f, iter->item);
        }
}

void log_state(FILE *f,
               int time,
               const struct list *joblist,
               const struct list *reglist)
{
        fprintf(f, "%d:%ld,%ld:", time, joblist->size, reglist->size);
        log_reglist(f, joblist);
        fputc(';', f);
        log_reglist(f, reglist);
        fputs(";\n", f);
}
