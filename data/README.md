# partiziok

## Programa konpilatzeko

Hurrengo elementuak beharrezkoak izango dira programa konpilatzeko:
- `gcc` edo C lengoairako konpiladore bat.
- **GSL** liburutegiak.
- `make` moduko programa proiektua eraikitzeko.

Programa konpilatzeko proiektuaren direktorioan kokatu eta ondorengo
agindua exekutatu:
```bash
$ make
```

## Programa erabiltzeko

Programa exekutatzen badugu, ondorengo mezua emango digu:
```bash
$ ./ff
arguments are: <dimensions> <part num> <part alpha> <part beta> <time range> <time alpha> <time beta> <seed> <verbose> [k]
```

Hainbat argumentu pasa behar dizkiogu. Argumentuak ondorengoak dira:
- `dimensions`: sarearen dimentsio kopurua.
- `part num`: sortu behar den partizio listaren luzera.
- `part alpha` eta `part beta`: partizioen tamaina kalkulatzeko erabiliko duen distribuzioaren parametroak.
- `time range`: partizioek har dezaketen denbora-tartea, [0, range] tartekoa.
- `time alpha` eta `time beta`: partizioen denbora kalkulatzeko erabiliko duen distribuzioaren parametroak.
- `seed`: partizioen lista sortzeko erabiliko den hazi-zenbakia.
- `verbose`: outputa pantailaratzea edo ez, balio boolearra.
- `k`: sareak dimentsio bakoitzean izango duen luzera. Hauek `dimensions` zenbaki izango dira.

Adibidez, 3 dimentsioko (3x3x4) sare batean sartuko diren 100 partizio, 
eta gehienez 1000 ziklo iraungo duten partizioen simulazioa egiteko, 
ondorengo paramentroak pasa ditzakegu (`seed`-aren arabera programaren 
irteera aldatuko da):

```bash
$ ./ff 3 100 2 5 1000 2 2 0 f 3 3 4
```

Aurreko aginduaren irteera honakoa da:
```bash
3_100_2_5_1000_2_2_0_3_3_4.txt
lost edges: 0
 num edges: 75
node utilization: 1.429854
edge utilization: 1.854156
```

Eta grafoaren egoera bakoitza ikusi dezakegu `3_100_2_5_1000_2_2_0_3_3_4.txt` fitxategian:
```bash
$ cat 3_100_2_5_1000_2_2_0_3_3_4.txt
...
```

## Programa borratzeko

```bash
$ make clean
```

## Programa estatikoki konpilatzeko

```bash
$ make static
```
