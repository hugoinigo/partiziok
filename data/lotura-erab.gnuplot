#!/usr/bin/gnuplot
reset

# png
#set terminal pngcairo size 800,600 enhanced font 'Verdana,10'
#eps
set terminal eps font 'Verdana,10'
set output 'lotura-erabilpena.eps'

# color definitions
set border linewidth 1.5
#set style line 1 lc rgb '#ff4000' pt 7 ps 1.5 lt 1 lw 3 # --- brick
set style line 10 lc rgb '#000000' dt 1 lt 1 lw 2 # --- magenta

#left bmargin auto column Left width -2.5

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.75
# Grid
set style line 12 lc rgb '#404040' lt 0 lw 0.5
set grid front ls 12

set ylabel "Erabilpen-tasa"
set yrange [0:1]
set ytic 0.1

set xlabel "Gertaera"
set xrange[1:]
set xtic auto

set key top right box lw 1 autotitle columnheader height 1

set datafile separator comma

# define starting point
plot 'lotura-erab.csv' u 1:3  w fillsteps fs solid .5 ls 3,\
     ''                u 1:3  w steps notitle ls 3,\
     ''                u 1:2 w lines ls 10
