#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
        double node_ut, edge_ut, link_ut;
        double node_aux, edge_aux, link_aux;
        double node_delta, edge_delta, link_delta;
        
        unsigned long time, time_delta, prev_time = 0;
        int res, count = 1;

        node_aux = 0.0;
        edge_aux = 0.0;
        link_aux = 0.0;

        FILE *er = fopen("nodo-erab.csv", "w+");
        FILE *bb = fopen("nodo-erab-bb.csv", "w+");

        fprintf(er, "\"Denbora\",\"Denbora-delta\",\"Nodo-erabilpena\",\"Ertz-erabilpena\"");
        fprintf(bb, "\"Denbora\",\"Nodo-bb.\",\"Ertz-bb.\"");

        while (1) {
                res = scanf("%lu %lf %lf\n", &time, &node_ut, &edge_ut);
                switch (res) {
                case EOF:
                        fprintf(er, "%ld,%ld,%lf,%lf\n", prev_time, time_delta, node_delta, edge_delta);
                        fprintf(bb, "%ld,%lf,%lf\n", prev_time, node_ut / time, edge_ut / time);
                        goto end;
                        break;
                case 0:
                        fprintf(stderr, "error while parsing line `%d'. Aborting.\n", count);
                        exit(1);
                        break;
                default:
                        node_delta = node_ut - node_aux;
                        edge_delta = edge_ut - edge_aux;
                        time_delta = time - prev_time;

                        fprintf(er, "%ld,%ld,%lf,%lf\n", prev_time, time_delta, node_delta, edge_delta);
                        fprintf(bb, "%ld,%lf,%lf\n", prev_time, node_ut / time, edge_ut / time);
                        
                        prev_time = time;
                        node_aux = node_ut;
                        edge_aux = edge_ut;
                        break;
                }
                count++;               
        }

 end:
        fclose(er);
        fclose(bb);
}
