#!/usr/bin/gnuplot
reset

# png
#set terminal pngcairo size 800,600 enhanced font 'Verdana,10'
# eps
set terminal eps font 'Verdana,10'
set output 'nodo-erabilpena.eps'

# color definitions
set border linewidth 1.5
#set style line 1 lc rgb '#8000ff' pt 7 ps 1.5 lt 1 lw 2 # --- purple
set style line 10 lc rgb '#000000' dt 1 lt 1 lw 2 # --- black 

#left bmargin auto column Left width -2.5

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.75
# Grid
set style line 12 lc rgb '#404040' lt 0 lw 0.5
set grid front ls 12

set ylabel "Erabilpen-tasa"
set yrange [0:1]
set ytic 0.1

set xlabel "Denbora-zikloa"
set xrange[1:]

set key top right box lw 1 autotitle columnheader height 1

set datafile separator comma

# define starting point
plot 'nodo-erab.csv'    u 1:($3/$2) w fillsteps fs solid .5 ls 1,\
     ''                 u 1:($3/$2) w steps notitle ls 1 lw 2,\
     'nodo-erab-bb.csv' u       1:2 w lines ls 10,\
