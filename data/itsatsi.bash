#!/usr/bin/env bash

delim=' '
data=$(head -n -22 $1)
utilization=$(sed "1d;s/^.*nu \(.*\) eu \(.*\) lu \(.*\) t \(.*\) p.*$/\4${delim}\1${delim}\2${delim}/" <<< ${data} | uniq)

./rf <<< ${utilization}

