#!/bin/env python3
# n-dimensional bin packing generalization using free submesh lists
# A free submesh list approach for n-dimensional bin packing problem
from copy import copy

class Reg:
    def __init__(self, coords, dims):
        self.x, self.y = coords
        self.w, self.h = dims

    def __repr__(self):
        return f"{{c{self.x}, {self.y}, d{self.w}, {self.h}}}"

    def __copy__(self):
        return Reg((self.x, self.y), (self.w, self.h))

    # self is subregion of reg
    def is_subregion(self, reg):
        c1 = (self.x >= reg.x) and (self.x-reg.x <= reg.w-self.w)
        c2 = (self.y >= reg.y) and (self.y-reg.y <= reg.h-self.h)
        return c1 and c2

    def overlaps(self, reg):
        c1 = (self.x-reg.x < reg.w and reg.x-self.x < self.w)
        c2 = (self.y-reg.y < reg.h and reg.y-self.y < self.h)
        return c1 and c2

    def overlaps_any_dim(self, reg):
        c1 = (self.x-reg.x < reg.w and reg.x-self.x < self.w)
        c2 = (self.y-reg.y < reg.h and reg.y-self.y < self.h)
        return c1, c2

    def is_partially_adj(self, reg):
        c1, c2 = self.overlaps_any_dim(reg)
        
        if c1 and ((self.y + self.h == reg.y) or (self.y == reg.y + reg.h)):
            return True, 1
        if c2 and ((self.x + self.w == reg.x) or (self.x == reg.x + reg.w)):
            return True, 0
        return False, 0
        

    def is_adjacent(self, reg):
        c1 = (self.x >= reg.x) and (self.x - reg.x <= reg.w - self.w)
        c2 = (self.y >= reg.y) and (self.y - reg.y <= reg.h - self.h)
        if c1 and ((self.y + self.h == reg.y) or (self.y == reg.y + reg.h)):
            return True, 1
        if c2 and ((self.x + self.w == reg.x) or (self.x == reg.x + reg.w)):
            return True, 0
        return False, 0

    def coords(self):
        return (self.x, self.y)

    def dims(self):
        return (self.w, self.h)

    def intersection(self, reg):
        x = max(self.x, reg.x)
        y = max(self.y, reg.y)
        w = min(self.x+self.w, reg.x+reg.w)
        h = min(self.y+self.h, reg.y+reg.h)

        return Reg((x, y), (w-x, h-y))

    def coadrants(self, reg):
        a = Reg((self.x, self.y), (reg.x-self.x, self.h))
        b = Reg((self.x, self.y), (self.w, reg.y-self.y))
        c = Reg((self.x, reg.y+reg.h), (self.w, (self.y+self.h)-(reg.y+reg.h)))
        d = Reg((reg.x+reg.w, self.y), ((self.x+self.w)-(reg.x+reg.w), self.h))

        ret = []
        if a.w != 0 and a.h != 0:
            ret.append(a)
        if b.w != 0 and b.h != 0:
            ret.append(b)
        if c.w != 0 and c.h != 0:
            ret.append(c)
        if d.w != 0 and d.h != 0:
            ret.append(d)
        return ret

class Mesh:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        self.job_count = 0
        self.mesh = [ [ 0 for i in range(w) ] for j in range(h) ]

    def __str__(self):
        s = ""
        for j in self.mesh:
            for i in j:
                s += "{} ".format("." if i == 0 else i)
            s += "\n"
                    
        return s
            
    def fill_reg(self, reg, id=0):
        self.job_count += 1
        if (id == 0):
            id = self.job_count

        for i in range(reg.w):
            for j in range(reg.h):
                self.mesh[reg.y+j][reg.x+i] = id

    def clear(self):
        self.job_count = 0
        for i in range(self.w):
            for j in range(self.h):
                self.mesh[j][i] = 0

w = 8
h = 8

# job_list = [
#     (1, 2),
#     (3, 2),
#     (1, 3),
#     (2, 2),
#     (4, 1),
#     (4, 2)
# ]
job_list = [
    (2, 2),
    (3, 4),
    (2, 3),
    (2, 3)
]

mesh = Mesh(w, h)

init_gap = Reg((0,0),(w,h))
gap_list = [init_gap]

def next_job():
    next_job.i += 1
    return job_list[next_job.i]

next_job.i = -1

def sim(coords):
    global gap_list
    job_dims = next_job()
    gap = gap_list.pop(0) # Take the first
    if coords is None:
        job = Reg(gap.coords(), job_dims)
    else:
        job = Reg(coords, job_dims)
    print("job", job)
    
    gaps = []
    gap_list1 = []
    for g in gap_list:
        if job.overlaps(g):
            gaps.append(g)
        else:
            gap_list1.append(g)
    gap_list = gap_list1

    gaps.append(gap)
    print("gaps", gaps)
    print("gl", gap_list)
    
    new_coads = []
    for gap in gaps:
        reg = job.intersection(gap)
        coads = gap.coadrants(reg)
        print("coads", coads)

        # the """pythonic way"""
        coads1 = [ c for c in coads if len([ 1 for g in new_coads if c.is_subregion(g) ]) == 0 ]
        new_coads1 = [ g for g in new_coads if len([ 1 for c in coads1 if g.is_subregion(c) ]) == 0 ]
        new_coads = new_coads1 + coads1

    coads = []
    for c1 in new_coads:
        keep = True
        for c2 in gap_list:
            if c1.is_subregion(c2):
                print("subreg", c1, c2)
                keep = False
                break
        if keep:
            coads.append(c1)

    gap_list += coads
    return job

def dealloc(job):
    global gap_list

    adj_list = []
    part_adj_list = []
    for gap in gap_list:
        adj1, dim1 = job.is_adjacent(gap)
        adj2, dim2 = job.is_partially_adj(gap)
        if adj1:
            adj_list.append((gap, dim1))
        if adj2 and not adj1:
            part_adj_list.append((gap, dim2))

    print('adj_list', adj_list)
    aux_list = []
    for i in range(2):
        aux_list.append(copy(job))
    for adj, dim in adj_list:
        aux = aux_list[dim]
        if dim == 0:
            a = min(aux.x, adj.x)
            b = max(aux.x+aux.w, adj.x+adj.w)
            aux.x = a
            aux.w = b - a
        else:
            a = min(aux.y, adj.y)
            b = max(aux.y+aux.h, adj.y+adj.h)
            aux.y = a
            aux.h = b - a
    for adj, dim in part_adj_list:
        aux = aux_list[dim]
    print('adj_list', adj_list)
    print('part_adj_list', part_adj_list)
    print('aux_list', aux_list)

    for aux in aux_list:
        for adj, _ in adj_list:
            is_adj, dim = aux.is_adjacent(adj)
            if not is_adj:
                is_adj, dim = aux.is_partially_adj(adj)
            if is_adj:
                if dim == 0:
                    a = min(aux.x, adj.x)
                    b = max(aux.x+aux.w, adj.x+adj.w)
                    aux.x = a
                    aux.w = b - a
                else:
                    a = min(aux.y, adj.y)
                    b = max(aux.y+aux.h, adj.y+adj.h)
                    aux.y = a
                    aux.h = b - a
    return aux_list

# print("gap_list", gap_list)
# 
# job1 = sim((3, 3))
# gap_list.append(gap_list.pop(0))
# print("gap_list", gap_list)
# for r in gap_list:
#     mesh.fill_reg(r)
# print(mesh)
# print(dealloc(job1))

gl = [
    (0,0,3,2),
    (0,0,2,6),
    (0,1,6,1),
    (0,4,6,2),
    (4,1,2,7)
]
gap_list = [Reg((x,y), (w, h)) for x, y, w, h in gl]
job1 = Reg((2, 2), (2, 2))
print(gap_list)

dealloc(job1)

# # exit(0)
# sim()
# mesh.clear()
# print("gap_list", gap_list)
# for r in gap_list:
#     mesh.fill_reg(r)
# print(mesh)
# 
# gap_list.reverse()
# sim()
# mesh.clear()
# print("gap_list", gap_list)
# for r in gap_list:
#     mesh.fill_reg(r)
# print(mesh)
# 
# gap_list.insert(0, gap_list.pop(1))
# sim()
# mesh.clear()
# print("gap_list", gap_list)
# for r in gap_list:
#     mesh.fill_reg(r)
# print(mesh)
# 

