#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <sys/types.h>

/* parse a single argument */
void parse_arg(const char* arg, long long* result)
{
        char* error;

        assert(*arg != '\0' && "empty string");

        *result = strtoll(arg, &error, 10);
        assert(*error == '\0' && "error occurred parsing");

        if (*result == ERANGE) {
                perror("strtoll");
                exit(1);
        }
        assert(*result >= 2 && "parsed number is smaller than 2");
}

/* parse an array of arguments */
void parse_argv(const size_t len,
                const char** argv,
                unsigned long* dim_list)
{
        long long result;
        const char* arg;
        size_t i;

        for (i = 0; i < len; ++i) {
                arg = argv[i];
                parse_arg(arg, &result);

                dim_list[i] = (unsigned long) result;
        }
}

unsigned long calculate_total_nodes(const size_t len,
                                    const unsigned long* dim_list)
{
        size_t i;
        unsigned long result = 1;
        for (i = 0; i < len; ++i) {
                result *= dim_list[i];
        }

        return result;
}

unsigned long calculate_inner_nodes(const size_t len,
                                    const unsigned long* dim_list)
{
        size_t i;
        unsigned long result = 1;
        for (i = 0; i < len; ++i) {
                result *= dim_list[i] - 2;
        }

        return result;
}

void print_results(unsigned long total_nodes,
                   unsigned long border_nodes,
                   unsigned long inner_nodes)
{
        printf("total nodes: %lu\n", total_nodes);
        printf("border nodes: %lu\n", border_nodes);
        printf("inner nodes: %lu\n", inner_nodes);
}

int main(int argc, const char* argv[])
{
        size_t len = argc - 1;
        unsigned long dimension_list[len],
                total_nodes,
                inner_nodes,
                border_nodes;

        if (argc <= 1) {
                printf("no arguments provided\n");
                return 0;
        }

        ++argv; // ignore the first argument (program name)
        parse_argv(len, argv, dimension_list);

        total_nodes = calculate_total_nodes(len, dimension_list);
        inner_nodes = calculate_inner_nodes(len, dimension_list);

        assert(inner_nodes < total_nodes);
        border_nodes = total_nodes - inner_nodes;

        print_results(total_nodes, border_nodes, inner_nodes);

        return 0;
}
