;;; emaitza baten eredua
;; (:name "4_10_2_2_100_2_2_1111_50_10_20_80.txt"
;;  :params (:dims 4
;;           :part-num 10
;;           :part-params (2 . 2)
;;           :time-range 100
;;           :time-params (2 . 2)
;;           :seed 1111
;;           :dims-size '(50 10 20 80))
;;  :utilization (:node 0.102103
;;                :edge 0.015894
;;                :part 0.264676)       
;;  :size (:min 15 :max 92416 :avg 12099.6)
;;  :time (:min 28 :max 92 :avg 65.0)
;;  :wait (:min 0 :max 0 :avg 0))

;; emaitza/eraikitze-funtzioak
(defun metric (min max avg)
  (list :min min
        :max max
        :avg avg))

(defun utilization (node edge part)
  (list :node node
        :edge edge
        :link part))

(defun result (name utilization metrics)
  (append 
   (list :name name
         :params (parse-filename name)
         :utilization utilization) metrics))

;; parser funtzioak
(defun split-at (sequence &key key (offset 0))
  (let* ((len (length sequence))
         (pos (or (position key sequence) 0)))
    (subseq sequence
            (+ pos offset) len)))

(defun parse-line (line &key (key #\:) (offset 2))
  (read-from-string (split-at line
                              :key key
                              :offset offset)
                    nil nil))

(defun parse-num (line &key (key #\:) (offset 2))
  (let ((end (length line))
        (start (+ offset
                  (position key line))))
    (values
     (read-from-string line nil nil :start start :end end))))

(defun parse-file (stream)
  (defun line ()
    (parse-num (read-line stream)))
  (result (read-line stream)
          (utilization (line)
                       (line)
                       (line))
          (list
           :size (metric (line) (line) (line))
           :real-size (metric (line) (line) (line))
           :time (metric (line) (line) (line))
           :wait (metric (line) (line) (line))
           :alloc-fails (metric (line) (line) (line))
           :alloc-fails-rot (metric (line) (line) (line))
           :sim-time (line))))

(defun filename-parser (filename)
  (let ((start 0)
        (end (position #\_ filename)))
    (lambda ()
      (let ((val
              (read-from-string
               filename nil nil
               :start start
               :end end)))
        (when end
          (setf start (1+ end)
                end (or
                     (position #\_ filename :start start)
                     (position #\t filename :start start)))
          val)))))

(defun parse-filename (filename)
  (let* ((reader (filename-parser filename))
         (dims (funcall reader)))
    (list :dims dims
          :part-num (funcall reader)
          :part-params `(,(funcall reader) . ,(funcall reader))
          :time-range (funcall reader)
          :time-params `(,(funcall reader) . ,(funcall reader))
          :speed-factor (funcall reader)
          :seed (funcall reader)
          :max-size (funcall reader)
          :rotate (funcall reader)
          :part-policy (funcall reader)
          :dims-size (loop repeat dims collect (funcall reader)))))

;; IO funtzioak
(defglobal *db* nil)

(defun read-result (filename)
  (with-open-file (stream filename)
    (parse-file stream)))

(defun save-db (filename)
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-files (pn)
  (setf *db* (mapcar (lambda (pname)
                       (with-open-file (in pname)
                         (parse-file in)))
                     (directory pn))))

(defun load-db (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

(defun load-files* (pn)
  (mapcar #'(lambda (pname)
              (with-open-file (in pname)
                (parse-file in)))
          (directory pn)))

;; funtzio-matematikoak
(defmacro square (expr)
  `(* ,expr ,expr))

(defun mean (list)
  (loop
    for i in list
    counting t into length
    summing i into sum
    finally (return (/ sum length))))

(defun variance (list &optional mean)
  (let ((mean (or mean
                  (mean list))))
        ;; (len (length list)))
    (loop
      for i in list
      summing (square (- i mean)))))

(defun standard-deviation (list &optional variance)
  (let ((n (- (length list) 1))
        (variance (or variance
                      (variance list))))
    (sqrt (/ variance n))))

(defun calc-stats (list &optional (keywords nil))
  (let* ((m (mean list))
         (v (variance list m))
         (sd (standard-deviation list v)))
    (if keywords
        (list
         :mean m
         :standard-deviation sd)
        (list m sd))))

;; datubase-eragiketa funtzioak
;; (defun select (predicate sequence)
;;   (remove-if-not predicate sequence))

(defmacro rgetf (place &rest indicators)
  (if indicators
      `(getf (rgetf ,place ,@(butlast indicators)) ,@(last indicators))
      place))

(defun make-comparison-expr (field value)
  `(equal (rgetf result :params ,field) ,value))

(defun make-selector-expr (keywords)
  (cond
    ((listp keywords) `(rgetf result ,@keywords))
    (t `(rgetf result ,keywords))))

(defun make-comparisons-list (fields)
  (loop while fields
        collecting (make-comparison-expr (pop fields) (pop fields))))

(defun make-selector-list (keywords)
  (loop while keywords
              collecting (make-selector-expr (pop keywords))))

(defmacro where (&rest clauses)
  `#'(lambda (result)
       (and ,@(make-comparisons-list clauses))))

(defmacro from* (&rest keywords)
  `#'(lambda (result)
       (list ,@(make-selector-list keywords))))

(defmacro from (&rest keywords)
  `#'(lambda (result)
       ,@(make-selector-list (list keywords))))

(defun select (sequence &key w f)
  (let ((query (if w (remove-if-not w sequence)
                   sequence)))
    (if f (mapcar f query)
        query)))

(defun calc-results-stats (results)
  (let ((places '(:node :edge :link)))
    (append
     (loop
      for key in places
      append (list
              (calc-stats (select results :f (from :utilization key)))))
     (list (first (calc-stats (select results :f (from :sim-time))))))))

;; plot funtzioak
(defun cons-queries-k (n k)
  (loop for len in k
        append (loop for psize in '(txikia ertaina handia)
                     append (loop for tsize in '(laburra ertaina luzea)
                                  collect (list n len psize tsize)))))

(defun cons-queries (dims)
  (loop for dim in dims
        append (loop for psize in '(txikia ertaina handia)
                      append (loop for tsize in '(laburra ertaina luzea)
                                   collect (list dim psize tsize)))))

(defun cons-select-k (db params)
  (let ((k (make-list (first params) :initial-element (second params)))
        (psize (case (third params)
                 (txikia '(2 . 5))
                 (ertaina '(5 . 5))
                 (handia '(5 . 2))))
        (tsize (case (fourth params)
                 (laburra '(2 . 5))
                 (ertaina '(5 . 5))
                 (luzea '(5 . 2)))))
    `(select ,db :w (where :part-params (quote ,psize)
                           :time-params (quote ,tsize)
                           :dims-size (quote ,k)))))

(defun cons-select-by-policy (db policy)
  (let ((part-len-list '(("Txikia" (2 . 5))
                         ("Ertaina" (5 . 5))
                         ("Handia" (5 . 2))))
        (time-len-list '(("Laburra" (2 . 5))
                         ("Ertaina" (5 . 5))
                         ("Luzea" (5 . 2)))))
    (loop for part-len in
          part-len-list
          append
          (loop for time-len in
                  time-len-list
                collect
                `(,(first part-len)
                  ,(first time-len)
                  (select ,db :w
                          (where
                           :part-params (quote ,(second part-len))
                           :time-params (quote ,(second time-len))
                           :part-policy (quote ,policy))))))))

(defun results-by-policy (db policy)
  (let ((queries (cons-select-by-policy db policy)))
    (loop for query in queries
          collect
          (progn
            (setf (third query)
                  (calc-results-stats
                   (eval (third query))))
            query))))

(defun cons-select (params)
  (let ((dim (first params))
        (psize (case (second params)
                 (txikia '(2 . 5))
                 (ertaina '(2 . 2))
                 (handia '(5 . 2))))
        (tsize (case (third params)
                 (laburra '(2 . 5))
                 (ertaina '(2 . 2))
                 (luzea '(5 . 2)))))
    `(select *db* :w (where :dims ,dim
                            :part-params (quote ,psize)
                            :time-params (quote ,tsize)))))

(defun get-stats (select)
  (calc-results-stats (eval select)))

(defun stringify-query (place)
  (setf place (case place
                (txikia "Small")
                (ertaina "Medium")
                (handia "Big")
                (laburra "Short")
                (luzea "Long"))))

(defun get-results-k (db n k)
  (mapcar #'(lambda (query)
              (append (list
                       (first query)
                       (second query)
                       (stringify-query (third query))
                       (stringify-query (fourth query)))
                      (get-stats (cons-select-k db query))))
          (cons-queries-k n k)))

(defun get-results (dims)
  (mapcar #'(lambda (query)
              (append query
                      (get-stats (cons-select query))))
          (cons-queries dims)))

;; (defun get-selects (dims)
;;   (loop for dim in dims
;;                collect (loop for psize in '((2 . 5) (2 . 2) (5 . 2))
;;                             append (loop for tsize in '((2 . 5) (2 . 2) (5 . 2))
;;                                         collect `(select *db* :w (where :dims ,dim :part-params (quote ,psize) :time-params (quote ,tsize)))))))

(defun regroup-results-k (dims results)
  (loop for dim in dims
        collect (cons dim
                      (mapcar #'(lambda (q) (remove (setf (nth 1 q) (gensym)) q))
                              (remove-if-not #'(lambda (res)
                                                 (eq dim (cadr res)))
                                             results)))))
;; (defun regroup-results-1 (dims results)
;;   (let ((res (mapcar #'(lambda (q)
;;                          (cons (cadr q) (delete (cadr q) q)))
;;                      results)))
;;     (regroup-results dims res)))

(defun regroup-results (dims results)
  (loop for dim in dims
        collect (cons dim
                      (mapcar #'(lambda (q) (rest q))
                              (rest (remove-if-not #'(lambda (res)
                                   (eq dim (car res)))
                                                   results))))))

(defun format-results-k (results)
  (let ((filename (pathname (format nil "dim~d-~d.csv"
                                    (caadr results)
                                    (first results)))))
    (with-open-file (out filename :direction :output :if-exists :supersede)
      (with-standard-io-syntax
        (format out "\"Parameters\",\"Node-utilization\",\"Node-error\",\"Links-utilization\",\"Links-error\"~%")
        (format out "~{~{~*\"~a-~a\"~{,~f~}~{,~f~}~}~%~}" (rest results))))))
                                    

(defun format-results (results)
  (let ((filename (pathname (format nil "dim~d.csv" (first results)))))
    (with-open-file (out filename :direction :output :if-exists :supersede)
      (with-standard-io-syntax
        (format out "\"Paramentroak\",\"Nodo-erabilera\",\"Nodo-errorea\",\"Lotura-erabilpena\",\"Lotura-errorea\"~%")
        (format out "~{~{\"~a - ~a\"~{,~f~}~{,~f~}~}~%~}" (rest results))))))

(defun format-results-by-policy (db policy id)
  (let ((results (results-by-policy db policy))
        (filename (format nil "~a-~a.csv" policy id)))
    (with-open-file (out filename :direction :output :if-exists :supersede)
      (with-standard-io-syntax
        (format out "\"Parametroak\",\"Nodo-erab\",\"$\\sigma$ (N)\",\"Ertz-erab\",\"$\\sigma$ (E)\",\"Lotura-erab\",\"$\\sigma$ (L)\",\"Ziklo-kop\"~%")
        (format out "~{~{\"~a-~a\"~{~{,~f,~f~}~{,~f,~f~}~{,~f,~f~},~f~}~}~%~}" results)))))

(defun format-results-list-k (results-list)
  (loop for results in results-list
        do (format-results-k results)))

(defun format-results-list (results-list)
  (loop for results in results-list
        do (format-results results)))

(defun load-databases ()
  (defglobal *3d* (load-files #P"../3d/*.txt"))
  (defglobal *4d* (load-files #P"../4d/*.txt"))
  (defglobal *6d* (load-files #P"../6d/*.txt")))

(defun main ()
  (loop for db in '(*3d* *4d* *6d*)
        for name in '(3d 4d 6d)
        do (loop for policy in '(max-dim min-dim)
                 do (format-results-by-policy db policy name))))
                     
