#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define ALPHA 2
#define BETA 5

/* long min(long a, long b)
{
        return a < b ? a : b;
}

long vector_nodes(const long *vec, const size_t size)
{
        size_t i;
        long dim = 1;
        for (i = 0; i < size; ++i) {
                dim *= vec[i];
        }

        return dim;
}

void gen_vec(const gsl_rng *r,
             long *vec,
             const long n,
             const long max_dim)
{
        size_t i;
        for (i = 0; i < n; ++i) {
                vec[i] = generate_number(r, max_dim);
        }
} */

void parse_arg(const char *arg, long *result)
{
        char *error;
        assert(*arg != '\0' && "empty string");

        *result = strtol(arg, &error, 10);
        assert(*error == '\0' && "error occurred parsing");

        if (*result == ERANGE) {
                perror("strtol");
                exit(1);
        }
}

long generate_number(const gsl_rng *r, const long max_dim)
{
        long result = gsl_ran_beta(r, ALPHA, BETA) * max_dim;
        if (result == 0)
                result = 1;
        return result;
}

void printl(const long *vec, const size_t n)
{
        size_t i;
        for(i = 0; i < n; ++i) {
                printf("%li ", vec[i]);
        }
        printf("\n");
}

void generate_vector(const gsl_rng *r,
                      long *vec,
                      const long n,
                      const long k,
                      const long time)
{
        size_t i;

        for (i = 0; i < (n-1); ++i) {
                vec[i] = generate_number(r, k);
        }
        vec[n-1] = generate_number(r, time);
        printl(vec, n);
}

void generate_vector_2(const gsl_rng *r,
                      long *vec,
                      const long n,
                      const long k,
                      const long time)
{
        size_t i;

        for (i = 0; i < (n-1); ++i) {
                vec[i] = generate_number(r, k);
        }
        vec[n-1] = generate_number(r, time);
        printl(vec, n);
}

int main(int argc, char *argv[])
{
        long n, k;
        long size, time_range;
        size_t i;

        long **dim_list, *vec;

        gsl_rng *r;

        if (argc != 5) {
                fprintf(stderr, "arguments are: <dimensions> <length> <count> <range>\n");
                return 1;
        }

        parse_arg(argv[1], &n);
        parse_arg(argv[2], &k);
        parse_arg(argv[3], &size);
        parse_arg(argv[4], &time_range);

        assert(n >= 1 && "n is less than 1");
        assert(k >= 2 && "k is less than 2");
        assert(size >= 1 && "count is less than 1");
        assert(time_range >= 1 && "range is less than 1");

        dim_list = malloc(sizeof(long *) * size);
        if (dim_list == NULL) {
                perror("malloc");
                return 2;
        }

        r = gsl_rng_alloc(gsl_rng_taus);

        /* We set the seed with our current time */
        gsl_rng_set(r, time(NULL));

        for (i = 0; i < size; ++i) {
                vec = malloc(sizeof(k) * (n+1));
                /* We don't check for malloc errors who cares */
                generate_vector(r, vec, n+1, k, time_range);

                dim_list[i] = vec;
                printl(vec, n+1);
        }

        /* Free all the memory */
        for (i = 0; i < size; ++i) {
                free(dim_list[i]);
        }

        gsl_rng_free(r);
        free(dim_list);
        return 0;
}
