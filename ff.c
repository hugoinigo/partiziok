#include "ff.h"
#include "gen.h"
#include "graph.h"
#include "part.h"
#include "edges.h"
#include "permutation.h"
#include "metric.h"
#include "array.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <float.h>

static double total_alloc = 0;
static double total_dealloc = 0;

/* Check if the partition at the given coords fits in the
 * given graph. If the partition fits, returns true,
 * false otherwise.
 */
static bool part_fits(const struct graph *g,
                      const long *coords,
                      const long *part)
{
        unsigned long i, res;
        long *iter = calloc(sizeof(*iter), g->n);

        res = 1;
        for (i = 0; i < g->n; ++i) {
                res *= part[i];
        }

        for (i = 0; i < res; ++i) {
                if (*graph_elem(g, coords, iter) != FREE) {
                        free(iter);
                        return false;
                }
                iter_coords(iter, part, g->n);
        }

        free(iter);
        return true;
}

/*
 * If the given partition fits in the graph,
 * we construct a partition struct to keep track of it.
 * If it does not fit, we return NULL
 */
static struct part *try_fit(struct graph *g,
                            const long *coords,
                            const long *part,
                            const unsigned long id)
{
        size_t i;
        struct part *result;

        for (i = 0; i < g->n; ++i) {
                if (coords[i] + part[i] > g->k[i]) {
                        return NULL;
                }
        }

        if (!part_fits(g, coords, part)) {
                return NULL;
        }

        set_graph(g, coords, part, id);

        result = malloc(sizeof(struct part));

        result->coords = malloc(sizeof(unsigned long) * g->n);
        result->dimensions = malloc(sizeof(unsigned long) * g->n);

        result->time = part[g->n];
        result->next = NULL;
        result->links = part_count_edges(part, g->n);

        for (i = 0; i < g->n; ++i) {
                result->coords[i] = coords[i];
                result->dimensions[i] = part[i];
        }

        return result;
}

static unsigned long part_min_time(const struct part *const p)
{
        unsigned long min = BIG_NUM;
        const struct part *i = p;

        while (i != NULL) {
                if (i->time < min) {
                        min = i->time;
                }
                i = i->next;
        }

        return min;
}

static void part_dec_time_by(struct part *const p, unsigned long num)
{
        struct part *i = p;
        while (i != NULL) {
                i->time -= num;
                i = i->next;
        }
}

static unsigned long count_part_nodes(const struct part *p, const unsigned long n)
{
        unsigned long i, acc = 1;
        for (i = 0; i < n; ++i) {
                acc *= p->dimensions[i];
        }

        return acc;
}

/*
unsigned long count_allocated_nodes(const struct part *h, const unsigned long n)
{
        const struct part *i = h;
        unsigned long acc = 0;
        unsigned long j;

        while (i != NULL) {

                acc += count_part_nodes(i, n);
                i = i->next;
        }

        return acc;
}
*/

static void update_utilization(struct graph *g,
                               struct part *const *head)
{
        double cur_node_utilization, cur_edge_utilization;

        /* nodoen erabilpena */
        cur_node_utilization = (g->time - g->prev_time)
                * g->alloc_nodes / (double) g->len;
        g->node_utilization += cur_node_utilization;
        /* printf("time: %lu prev_time: %lu alloc_nodes: %lu len: %lu cur util: %lf\n", */
        /*        g->time, g->prev_time, g->alloc_nodes, g->len, cur_node_utilization); */

        /* ertzen erabilpena */
        cur_edge_utilization = (g->time - g->prev_time)
                * g->alloc_edges / (double) g->e;
        g->edge_utilization += cur_edge_utilization;

        /* alloc_and_lost_edges = (double) (g->lost_edges + */
        /*                                  (g->alloc_edges ? g->alloc_edges : 1)); */
        /* g->edge_loss += g->lost_edges / alloc_and_lost_edges; */
        /* printf("[U] lost_edges: %lu alloc_edges: %lu event: %lu\n", */
        /*        g->lost_edges, g->alloc_edges, g->event); */
        /* fprintf(stdout, "part utilization: %lf -> %lu / %lu\n", g->edge_loss, g->lost_edges, g->alloc_edges); */
}

static void update_link_loss_utilization(struct graph *g)
{
        double alloc_and_lost_edges;
        alloc_and_lost_edges = (double) (g->lost_edges + g->alloc_edges);

        g->edge_loss += g->lost_edges / (alloc_and_lost_edges ? alloc_and_lost_edges : 1);
        /* printf("[L] lost_edges: %lu alloc_edges: %lu event: %lu\n", */
        /*        g->lost_edges, g->alloc_edges, g->event); */
}

static double time_delta(struct timespec prev_ts, struct timespec post_ts)
{
	double result = post_ts.tv_sec - prev_ts.tv_sec;
	result += (post_ts.tv_nsec - prev_ts.tv_nsec) / 1000000000.0;
	return result;
}

static void part_remove_timeouts(FILE *restrict s,
                                 const struct config *c,
                                 struct part **head,
                                 struct graph *g)
{
        struct part **i = head;
        struct part *next;
        unsigned lost_edges;
        double dealloc_delta;
        struct timespec begin, end;

        while (*i != NULL) {
                if ((*i)->time <= 0) {
                        lost_edges = count_lost_edges(*i, g);

                        update_link_loss_utilization(g);

                        g->alloc_nodes -= count_part_nodes(*i, g->n);
                        g->lost_edges -= lost_edges;
                        g->alloc_edges -= (*i)->links;

			if (c->verbose) {
				printf("[DEALLOC] job\t");
				fflush(stdout);
			}
                        clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
                        set_graph(g, (*i)->coords, (*i)->dimensions, FREE);
                        clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                        dealloc_delta = time_delta(begin, end);
			if (c->verbose) {
				printf("\tDONE (%f)\n", dealloc_delta);
			}

                        update_metricd(&dealloc_time, dealloc_delta);
                        total_dealloc += dealloc_delta;

                        ++g->event;
                        if (c->log) {
                                fprintf(s, "D ");
                                fprintg(s, g);
                                fprintp(s, *i, g->n);
                        }

                        next = (*i)->next;
                        part_free(*i);
                        *i = next;
                } else {
                        i = &(*i)->next;
                }
        }
}

/*
unsigned long calculate_lost_edges(const struct config *c)
{
        unsigned long result = 0;
        unsigned long part_res;
        for (unsigned i = 0; i < c->n; ++i) {
                part_res = 2;
                for (unsigned j = 0; j < c->n-1-i; ++j) {
                        part_res *= c->k[j];
                }
                for (unsigned l = c->n-i; l < c->n; ++l) {
                        part_res *= c->k[l];
                }
                result += part_res;
        }

        return result;
}
*/

static void fprintm(FILE *restrict s,
                    const char *msg,
                    const struct metric *m)
{
        /* fprintf(s, "%s metric:\n", msg); */
        fprintf(s, "%s min: %lu\n", msg,
                m->min == ULONG_MAX ? 0 : m->min);
        fprintf(s, "%s max: %lu\n", msg, m->max);
        fprintf(s, "%s avg: %lf\n", msg, m->avg);
}

static void fprintmd(FILE *restrict s,
		     const char *msg,
		     const struct metricd *m)
{
        /* fprintf(s, "%s metric:\n", msg); */
        fprintf(s, "%s min: %lf\n", msg,
                m->min == DBL_MAX ? 0 : m->min);
        fprintf(s, "%s max: %lf\n", msg, m->max);
        fprintf(s, "%s avg: %lf\n", msg, m->avg);
}

int first_fit(const struct config *cfg)
{
        long **part_list;
        unsigned long i, j;

        struct graph g;
        struct part *part;

        long *coords;
        unsigned long delta_time;

        struct part *head = NULL;

        bool is_full;
        char *fname;
        FILE *fd;

        unsigned long part_nodes, lost_edges;
        unsigned long atrials;

        unsigned long max_permu, pind;
        long *permu;

        double alloc_delta = 0;
        struct timespec begin, end;

        fname = gen_filename(cfg);
        fd = fopen(fname, "w+");

        /* fprintf(fd, "%s\n", fname); */

        part_list = gen_job_list(cfg);

        graph_alloc(&g, cfg->k, cfg->n);
        coords = malloc(sizeof(*coords) * cfg->n);

        if (cfg->rotate) {
            max_permu = factorial(cfg->n);
        } else {
            max_permu = 1;
        }

        permu = malloc(sizeof(*permu) * (cfg->n + 1));
        atrials = 0;

        for (i = 0; i < cfg->part_list_size; ++i) {
                is_full = true;
                g.prev_time = g.time;
                permu[cfg->n] = part_list[i][cfg->n];
		if (cfg->verbose) {
			printf("[ALLOC] job id: %ld", i+1);
			fflush(stdout);
		}
                for (pind = 0; pind < max_permu; ++pind) {
                        /* we set the coords to 0 for every partition */
                        for (j = 0; j < cfg->n; ++j) {
                                coords[j] = 0;
                        }
                        lexicographical_permutation(permu, pind, part_list[i], cfg->n);
                        do {
				clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
                                part = try_fit(&g, coords, permu, i+1);
				clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                                alloc_delta += time_delta(begin, end);

                                if (part == NULL) {
					clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
                                        iter_coords(coords, cfg->k, cfg->n);
					clock_gettime(CLOCK_MONOTONIC_RAW, &end);
					alloc_delta += time_delta(begin, end);
                                } else {
					if (cfg->verbose) {
						printf("\tDONE (%f)\n", alloc_delta);
					}
                                        /* a partition has been assigned */
                                        g.time += 1;
                                        /* calculate part utilization */
                                        update_utilization(&g, &head);
                                        update_link_loss_utilization(&g);

                                        update_metric(&afails_rot, pind);
                                        update_metric(&afails, atrials);
                                        atrials = 0;

                                        part_nodes = count_part_nodes(part, g.n);
                                        lost_edges = count_lost_edges(part, &g);

                                        /* update job size metrics */
                                        update_metric(&psize, part_nodes);

                                        /* update job time metrics */
                                        update_metric(&ptime, part->time);

                                        /* update allocation time metrics */
                                        update_metricd(&alloc_time, alloc_delta);
                                        total_alloc += alloc_delta;
                                        alloc_delta = 0;

                                        g.alloc_nodes += part_nodes;
                                        g.alloc_edges += part->links;
                                        g.lost_edges += lost_edges;

                                        part_append(&head, part);

                                        /* decrease 1 */
                                        part_dec_time_by(head, 1);

                                        ++g.event;
                                        if (cfg->log) {
                                                fprintf(fd, "A ");
                                                fprintg(fd, &g);
                                                fprintp(fd, part, g.n);
                                        }

                                        is_full = false;
                                        break;
                                }
                        } while (!array_all(coords, g.n, 0));
                        if (!is_full) {
                                break;
                        }
                }

                if (is_full) {
			if (cfg->verbose) {
				printf("\tWAITING (%ld)\n", i+1);
			}
                        /* calculate next time */
                        delta_time = part_min_time(head);

                        /* update job wait metric */
                        update_metric(&pwait, delta_time);

                        /* decrease time */
                        part_dec_time_by(head, delta_time);

                        i--; /* try to fit the last */
                        g.time += delta_time;
                        atrials++;
                        update_utilization(&g, &head);
                }

                /* remove old parts */
                part_remove_timeouts(fd, cfg, &head, &g);
        }

        while (head != NULL) {

                delta_time = part_min_time(head);
                part_dec_time_by(head, delta_time);

                g.prev_time = g.time;
                g.time += delta_time;

                update_utilization(&g, &head);
                part_remove_timeouts(fd, cfg, &head, &g);
        }
        double total_time = total_alloc + total_dealloc;
        alloc_time.avg = total_alloc / cfg->part_list_size;
	dealloc_time.avg = total_dealloc / cfg->part_list_size;

        printf("%s\n", fname);
	printf("node utilization:   %lf\n", g.node_utilization / g.time);
	printf("total alloc time:   %lf\n", total_alloc);
	printf("total dealloc time: %lf\n", total_dealloc);
	printf("total time:         %lf\n", total_time);
	printf("alloc time avg:     %lf\n", alloc_time.avg);
	printf("dealloc time avg:   %lf\n", dealloc_time.avg);

	fprintf(fd, "%s\n", fname);
        fprintf(fd, "node utilization: %lf\n", g.node_utilization / g.time);
        fprintf(fd, "edge utilization: %lf\n", g.edge_utilization / g.time);
        fprintf(fd, "part utilization: %lf\n", g.edge_loss / (2 * cfg->part_list_size));

        psize.avg /= cfg->part_list_size;
        fprintm(fd, "part size", &psize);
        
        ifrag.avg /= cfg->part_list_size;
        fprintm(fd, "real part size", &ifrag);

        ptime.avg /= cfg->part_list_size;
        fprintm(fd, "part time", &ptime);
        
        pwait.avg /= cfg->part_list_size;
        fprintm(fd, "part wait", &pwait);

        afails.avg /= cfg->part_list_size;
        fprintm(fd, "alloc fails", &afails);

        afails_rot.avg /= cfg->part_list_size;
        fprintm(fd, "alloc fails rot", &afails_rot);

        fprintf(fd, "simulation time: %lu\n", g.time);

	fprintf(fd, "alloc time: %lf\n", total_alloc);
	fprintmd(fd, "alloc time", &alloc_time);

	fprintf(fd, "dealloc time: %lf\n", total_dealloc);
	fprintmd(fd, "dealloc time", &dealloc_time);

        fprintf(fd, "total time: %lf\n", total_time);

        free(fname);
        fclose(fd);

        part_free_all(&head);
        graph_free(&g);

        free(coords);
        free(permu);
        gen_free_job_list(part_list, cfg->part_list_size);

        return 0;
}

