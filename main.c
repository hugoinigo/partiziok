#include "util.h"
#include "args.h"

int main(int argc, char *argv[])
{
        struct config c;
        parse_args(&c, argc, argv);

        c.simulation(&c);
        free(c.k);

        return 0;
}
